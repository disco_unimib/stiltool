import os.path
import sys
from getpass import getpass
import random
import string
import signal

ENV_PROD_FILENAME = ".env"

class BaseEvaluator:
    def eval(self, what):
        return True
    
    def error(self):
        return ""
    
    def strict(self):
        return True
    
class IntegerEvaluator(BaseEvaluator):
    def __init__(self):
        super(IntegerEvaluator).__init__()
        
    def eval(self, what):
        try:
            int(what)
        except ValueError:
            return False
        
        return True
    
    def error(self):
        return "Non integer input"
    
    def strict(self):
        return True
    
class PositiveIntegerEvaluator(IntegerEvaluator):
    def __init__(self):
        super(PositiveIntegerEvaluator).__init__()
        
    def eval(self, what):
        is_integer = super().eval(what)
        return is_integer and int(what) > 0
    
    def error(self):
        return "Non positive integer input"
    
    def strict(self):
        return True
    
class PasswordEvaluator(BaseEvaluator):
    def __init__(self, username):
        super(PasswordEvaluator).__init__()
        self.username = username
        self.errors = []
        
    def eval(self, what):
        long_pwd = len(what) >= 8
        has_numbers = any(char.isdigit() for char in what)
        has_special_chr = any(char in string.punctuation for char in what)
        is_user_info_free = self.username.lower() not in what.lower()
        
        self.errors = []
        if not long_pwd:
            self.errors.append("Your password is too short (should have at least 8 characters)")
        if not has_numbers:
            self.errors.append("Your password should contains at least a number")
        if not has_special_chr:
            self.errors.append(f"Your password should contains at least one special character (e.g. {string.punctuation})")
        if not is_user_info_free:
            self.errors.append(f"Your password should not contains username")        
        
        return long_pwd and has_numbers and has_special_chr and is_user_info_free
    
    def error(self):
        return self.errors
    
    def strict(self):
        return False

def send(message):
    print(message)
    
def send_logo():
    logo = '''
                            ▄▓▓▓▄       
                           ▓▓▓▓█▀       
                ,╓╗▄▓▓▓▓▓▄ ▓▌░ ║╜▀▄,    
             ,▄███▓█▓▓▓▓▓██▓█K╨¼   `╙═. 
        ╓▄▄▓▓██▓██▓█▓▓███▓▓╜:╙╬M        
 ^"5▓██████████████▓▓███▀▀, ]░Å         
      «╝╨╨╨╨╨╜─«Ñ░╨╨░░░░"]╩Ñ╨           
                 ÿ╨╜┴≈─``               
                ╣   ╔⌐                  
              ╓█    ▓                   
              ╙╨╨╨╩╫R¥¥KKKΦ╗            
                   ╫        ╚╕          
                   ║                    
                   ]⌐                   
                   ]L                   
                   ]▌                   
                  .╩╩Ñ≡æ≥─              
    '''
    send(logo)

def ask(message, default_yes=False):
    yes_answer = "Y" if default_yes else "y"
    no_answer = "N" if not default_yes else "n"
    
    wrong_answer = True
    while wrong_answer:
        answer = input(f"{message}? [{yes_answer}/{no_answer}] ")
        
        if answer == "":
            answer = default_yes
        else:
            if answer.lower() == "y":
                answer = True
            elif answer.lower() == "n":
                answer = False
            else:
                print("Answer with 'y' or 'n'")
                continue
        
        wrong_answer = False               
                
    return answer

def ask_var(message, default=None, concealed=False, evaluator=None):
    if evaluator is None:
        evaluator = BaseEvaluator()
    
    if not concealed:
        method = input
    else:
        method = getpass
    
    ok = False
    
    while not ok:
        if default is not None:
            answer = method(f"{message} (default='{default}'): ")
        else:
            answer = method(f"{message}: ")
            
        if answer == "":
            answer = default
            
        ok = evaluator.eval(answer)
        if not ok:
            errors = evaluator.error()
            if isinstance(errors, list):
                print("\tError:", "\n\t       ".join(errors))
            else:
                print("\tError:", errors)
                
            if not evaluator.strict():
                ok = ask("Continue anyway", False)
    
    return answer

def write_settings(host, port, admin_user, admin_password, email_host, email_host_user, email_password, challenge_mode):
    def generate_key():
        generated_key = ''.join([
            random.SystemRandom().choice(string.ascii_letters + string.digits + string.punctuation)
            for _ in range(50)
        ])
        return generated_key
    
    with open(ENV_PROD_FILENAME, "w") as f:
        f.write(f"HOST={host}\n")
        f.write(f"PORT={port}\n")
        f.write(f"SECRET_KEY={generate_key()}\n")
        f.write(f"ADMIN_USERNAME={admin_user}\n")
        f.write(f"ADMIN_PASSWORD={admin_password}\n")
        f.write(f"EMAIL_HOST={email_host}\n")
        f.write(f"EMAIL_HOST_USER={email_host_user}\n")
        f.write(f"EMAIL_PASSWORD={email_password}\n")
        f.write(f"CHALLENGE_MODE={challenge_mode}\n")
        f.write(f"DEBUG_MODE=False\n")

def exit():
    send("")
    send("Bye bye! Have a nice day...")
    sys.exit(0)
    
def signal_handler(sig, frame):
    send("Stopped by user...")
    send("All operations cancelled")
    exit()

def main():
    send("+-------------------------------------+")
    send("| StilTool set server parameters tool |")
    send("+-------------------------------------+")
    
    send_logo()
    
    remove_last_env = False
    if os.path.isfile(ENV_PROD_FILENAME):   # if exists
        yes = ask("A server parameters file exists, do you want to override it", True)
        if yes:
            remove_last_env = True
        else:
            exit()
            
    host = ask_var("Set current host name or ip (e.g. stiltool.com or 123.123.123.123)")
    port = ask_var("Set current host port", default="80", evaluator=PositiveIntegerEvaluator())
    admin_user = ask_var("Set admin username", default="admin")
    
    admin_password = ask_var("Set admin password", default="admin", concealed=True, evaluator=PasswordEvaluator(admin_user))
    if admin_password != "admin":
        admin_password_again = ask_var("Reinsert admin password", default="admin", concealed=True)
    else:
        admin_password_again = "admin"
    
    email_host = ask_var("Set smtp email host (e.g. smtp.gmail.com) [optional]", default="")
    if email_host != "":
        email_host_user = ask_var("Set smtp email host user (e.g. my-smtp-service@mail.com)")
        email_password = ask_var("Set email password", concealed=True)
        email_password_again = ask_var("Reinsert email password", concealed=True)
    else:
        email_host_user = ""
        email_password = ""
        
    challenge_mode = ask("Set challenge mode", True)
    
    # -----------------------------------------------
    send("")
    send("Checking...")
    if host is None:
        send("Error: current host name not set")
        exit()
        
    if port is None:
        send("Error: current host port not set")
        exit()
    
    if admin_user is None:
        send("Error: admin username cannot be empty")
        exit()
        
    if admin_password is None:
        send("Error: admin password cannot be empty")
        exit()
        
    if admin_password != admin_password_again:
        send("Error: admin password does not match")
        exit()
        
    if email_host_user is None:
        send("Error: invalid user email host")
        exit()
        
    if email_host_user is not "":
        if email_host_user is not None and "@" not in email_host_user:
            send("Error: invalid user email host")
            exit()
            
        if email_host_user is not None and email_password != email_password_again:
            send("Error: Email password does not match")
            exit()
        
    send("Ok...")
    send("")
    
    send(f"Host name: {host}")
    send(f"Host port: {port}")
    send(f"Admin username: {admin_user}")
    send(f"Admin password: {len(admin_password) * '*'}")
    if email_host != "":
        send(f"Email host: {email_host}")
        send(f"Email host user: {email_host_user}")
        send(f"Email password: {len(email_password) * '*'}")
    send(f"Challenge mode: {challenge_mode}" )
    
    send("")
    yes = ask("Are these settings correct")
    if yes:
        if remove_last_env:
            os.remove(ENV_PROD_FILENAME)
        write_settings(host, port, admin_user, admin_password, email_host, email_host_user, email_password, challenge_mode)
        send(f"Wrote settings to {ENV_PROD_FILENAME}")
    else:
        send("Operation cancelled")
    exit()
    

if __name__ == '__main__':
    signal.signal(signal.SIGINT, signal_handler)
    main()

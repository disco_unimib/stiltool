echo "+----------------------------+"
echo "| STILTool Production Server |"
echo "+----------------------------+"

# Installing node modules
if [ ! -d "node_modules" ]; then
    echo ""
    echo "[ Installing node modules ]"
    npm install
fi

if [ $? -eq 0 ]; then
    echo ""
    echo "[ Starting server... ]"
    
    # Check environment variables file
    if [ ! -f ".env" ]; then
        echo ""
        echo ".env file missing. Please run 'python set_server_parameters.py' or manually write an .env file"
        exit -1
    fi
    
    docker-compose -fdocker-compose.yml down
    docker-compose -fdocker-compose.yml up --build 
else
    echo ""
    echo "An error occurred... exiting"
fi

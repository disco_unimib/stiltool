# STILTool: a Semantic Table Interpretation evaLuation Tool

STILTool is an open-source tool which allows automatic evaluation of the 
quality of semantic annotations made by semantic table interpretation approaches. 
STILTool provides a graphical interface allowing users to analyse the correctness of the annotations 
of  tabular  data.  The  tool  also  provides  a  set  of  statistics  in  order  to 
identify the most common error patterns.

![STILTool history section](https://bitbucket.org/disco_unimib/stiltool/raw/475a9069097eedff224bd9b4dc811b7b2f6e50ba/app/static/app/images/step-3.png)

![STILTool evaluation section](https://bitbucket.org/disco_unimib/stiltool/raw/475a9069097eedff224bd9b4dc811b7b2f6e50ba/app/static/app/images/step-4.png)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine.

### Prerequisites and installation

A [Docker](https://www.docker.com/) configuration is provided to facilitate installation. This is currently the recommendend and supported way to install STILTool on your machine. NodeJS/NPM is also required.

The first step is to run 
* npm install
in order to retrieve the required packages.

### App settings

STILTool need some configuration parameters before running.

A python wizard is integrated to help you set these parameters:

```
python set_server_parameters.py
```

You can also set the config file manually but in most cases this is not recommended:

 * create an ".env" file
 * copy the content of ".env-template" into ".env" and use it as template
 * set the variables as desired

**Variables**

* **HOST**: a string representing the host/domain name of this Django site.

* **PORT**: a positive integer representing the port of this Django site.

* **SECRET_KEY**: a secret key for a particular Django installation. This is used to provide cryptographic signing, and should be set to a unique, unpredictable alpha-numeric value of considerable length (e.g. 50 characters).


* **ADMIN_USERNAME**: username for the user created during the first setup.
* **ADMIN_PASSWORD**: password for the user created during the first setup.  
    If not provided a user, it wil be create by default as "admin", "admin"


* **EMAIL_HOST**: SMTP email host configuration.
* **EMAIL_HOST_USER**: SMTP email host user configuration.
* **EMAIL_PASSWORD**: SMTP password configuration  
    If a SMTP is not provided, the registration with email confirm will be skipped

* **CHALLENGE_MODE**:
    * True:
        * admin can promote any user to staff
        * admin and staff users can upload a new Gold Standard while regular users can only submit their annotations
        * a leaderboard for each GS and each task (CTA, CPA, CEA) is available for both authenticated users and guests
    * False
        * all users can upload GS and set them to be private or public


* DEBUG_MODE: a boolean that turns on/off debug mode. This must be set to False for a production environment. Default: False


### Run
To run the tool execute the following command:

```
./deploy.sh
```

## License

This project is licensed under the Apache License - see the [LICENSE](LICENSE) file for details



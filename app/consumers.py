from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
import json
from django.core.serializers.json import DjangoJSONEncoder

class InternalConsumer(WebsocketConsumer):
    def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = "internal_%s" % self.room_name

        print("Connected to " + self.room_group_name)

        # Join room group
        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name
        )

        self.accept()

    # Receive message from WebSocket
    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']

        print('message from client: ' + message)
        # TODO: not implemented yet!

    def job_complete(self, event):
        self.send(text_data=json.dumps(event))

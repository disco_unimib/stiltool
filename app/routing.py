from django.conf.urls import url

from . import consumers

websocket_urlpatterns = [
    url(r'^ws/internal/(?P<room_name>[^/]+)/$', consumers.InternalConsumer),
]

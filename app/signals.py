from django.dispatch import receiver
from django.db.models.signals import post_migrate
from django.contrib.auth.models import User
from app.models import UserProfile
from stiltool import settings
from .forms import UploadGSForm, UploadAnnotationForm
from .models import GoldenStandard, UserAnnotation, TasksEnum, GSScopeEnum
from django.core.files.uploadedfile import SimpleUploadedFile

from .tasks import load_annotation

import zipfile
from io import BytesIO
import os.path

def _load_ann(file_path, task, demo_user):
    with open(file_path, "rb") as f:
        content = f.read()
                
    parameters = {
        "gs_choice": "SemTab Challenge - Round4",  # I expect this guy to exist
        "task_choice": task.name,
    }
    files = {
        'annotation_file': SimpleUploadedFile("placeholder.csv", content),
    }

    ann_form = UploadAnnotationForm(demo_user, parameters, files)
    print(ann_form.is_valid(), ann_form.errors)
    if ann_form.is_valid():
        ann_file = ann_form.cleaned_data.get('annotation_file')
        gs_choice = ann_form.cleaned_data.get('gs_choice')
        task_choice = ann_form.cleaned_data.get('task_choice')

        try:
            gs = GoldenStandard.objects.filter(name=gs_choice)
        except GoldenStandard.DoesNotExist:
            gs = []

        gs = [
            g
            for g in gs
            if g.owner == demo_user or g.settings["scope"] == GSScopeEnum.public.value
        ]

        gs = gs[0]
        ann = UserAnnotation(
            owner=demo_user,
            gs=gs,
            task_type=task_choice,
            score={},
            loaded_annotations={},
            RWNa_statistics={},
            annotation_statistics={},
            annotations=ann_file,
            todo=True
        )
        ann.save()
        load_annotation(ann.id, task_choice.value)
    else:
        print(ann_form.errors)

@receiver(post_migrate)
def on_init_db(**kwargs):
    def _create_superuser():
        has_superuser = User.objects.filter(is_superuser=True).count() > 0
        if not has_superuser:
            if settings.ADMIN_USERNAME != '' and settings.ADMIN_PASSWORD != '':
                admin = User.objects.create_superuser(settings.ADMIN_USERNAME, email='', password=settings.ADMIN_PASSWORD)
            else:
                admin = User.objects.create_superuser('admin', email='', password='admin')
                
            admin.save()
            
            UserProfile(
                user=admin
            ).save()
            
    def _add_gs():
        if GoldenStandard.objects.filter(name="SemTab Challenge - Round4").exists():
            return
        
        admin = User.objects.filter(is_superuser=True)[0]
        
        base_path = os.path.join(settings.BASE_DIR, "data/test/GS")
        dir_names = ["Round1", "Round2", "Round3", "Round4", "T2Dv2", "Limaye200"]
        
        with open(os.path.join(base_path, "GS.zip"), "rb") as f:
            zipcontent = BytesIO(f.read())
            with zipfile.ZipFile(zipcontent, 'r') as zf:
                for dir_name in dir_names:
                    if GoldenStandard.objects.filter(name=dir_name, owner=admin).count() > 0:
                        continue
                    
                    if dir_name != "Limaye200":
                        cta_content = zf.read(os.path.join(dir_name, f"CTA_{dir_name}.csv"))
                        cpa_content = zf.read(os.path.join(dir_name, f"CPA_{dir_name}.csv"))
                        cea_content = zf.read(os.path.join(dir_name, f"CEA_{dir_name}.csv"))
                        
                        if dir_name != "T2Dv2":
                            # SemTab Challenge
                            parameters = {
                                "gs_name": f"SemTab Challenge - {dir_name}",
                                "scope_choice": "public",
                                "mode_choice": "info",
                                "cta_score_choice": "AHAP"
                            }

                            if dir_name == "Round1":
                                parameters["cta_score_choice"] = "PRF"

                        else:
                            # T2Dv2
                            parameters = {
                                "gs_name": f"{dir_name}",
                                "scope_choice": "public",
                                "mode_choice": "info",
                                "cta_score_choice": "PRF"
                            }
                        files = {
                            'cta_file': SimpleUploadedFile("cta.csv", cta_content),
                            'cpa_file': SimpleUploadedFile("cpa.csv", cpa_content),
                            'cea_file': SimpleUploadedFile("cea.csv", cea_content)
                        }
                    else:
                        # Limaye200
                        cta_content = zf.read(os.path.join(dir_name, f"CTA_{dir_name}.csv"))
                        parameters = {
                            "gs_name": f"{dir_name}",
                            "scope_choice": "public",
                            "mode_choice": "info",
                            "cta_score_choice": "PRF"
                        }
                        files = {
                            'cta_file': SimpleUploadedFile("cta.csv", cta_content),
                        }
                        
                    
                    gs_form = UploadGSForm(admin, parameters, files)
                    if gs_form.is_valid():
                        # TODO: For now I manually save the golden standard
                        gs_name = gs_form.cleaned_data.get('gs_name')
                        cta = gs_form.cleaned_data.get('cta_file')
                        cpa = gs_form.cleaned_data.get('cpa_file')
                        cea = gs_form.cleaned_data.get('cea_file')
                        tables = gs_form.cleaned_data.get('table_files')
                        scope = gs_form.cleaned_data.get('scope_choice')
                        mode = gs_form.cleaned_data.get('mode_choice')

                        print(gs_name, scope, mode)

                        GoldenStandard(
                            owner=admin,
                            name=gs_name,
                            settings={
                                "scope": scope,
                                "mode": mode
                            },
                            tasks_metadata=gs_form.metadata,
                            cea=cea,
                            cpa=cpa,
                            cta=cta,
                            tables=tables,
                        ).save()
                    else:
                        print(gs_form.errors)

    def _add_demo_account():
        if User.objects.filter(username="demo").count() > 0:
            return

        demo_user = User.objects.create_user('demo', email='demo@demo.com', password='demo')
        demo_user.save()
            
        UserProfile(
            user=demo_user
        ).save()

        base_path = os.path.join(settings.BASE_DIR, "data/test/Annotations/Round4")

        CEA_files = [os.path.join(os.path.join(base_path, "CEA"), f) for f in os.listdir(os.path.join(base_path, "CEA")) if os.path.isfile(os.path.join(os.path.join(base_path, "CEA"), f))]
        CPA_files = [os.path.join(os.path.join(base_path, "CPA"), f) for f in os.listdir(os.path.join(base_path, "CPA")) if os.path.isfile(os.path.join(os.path.join(base_path, "CPA"), f))]
        CTA_files = [os.path.join(os.path.join(base_path, "CTA"), f) for f in os.listdir(os.path.join(base_path, "CTA")) if os.path.isfile(os.path.join(os.path.join(base_path, "CTA"), f))]

        for f in CEA_files:
            _load_ann(f, TasksEnum.CEA, demo_user)

        for f in CPA_files:
            _load_ann(f, TasksEnum.CPA, demo_user)
        
        for f in CTA_files:
            _load_ann(f, TasksEnum.CTA, demo_user)

    _create_superuser()
    
    if not settings.CHALLENGE_MODE:
        print("Adding default gs")
        _add_gs()

        print("Adding demo account")
        _add_demo_account()

    post_migrate.disconnect(on_init_db)

from app.publisher.channel import Channel


class Internal(Channel):
    def __init__(self, channel_id):
        super().__init__(channel_id, "internal")

    @staticmethod
    def annotation():
        return Internal("annotation")

    def notify_job_complete(self, ann_id):
        self.send({
            'type': 'job_complete',
            'ann_id': ann_id,
        })

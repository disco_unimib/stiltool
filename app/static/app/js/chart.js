// Tooltip
Chart.defaults.global.tooltips.mode = 'index';
Chart.defaults.global.tooltips.intersect = true;
Chart.defaults.global.tooltips.titleFontFamily = 'Roboto, sans-serif';
Chart.defaults.global.tooltips.titleFontSize = 16;
Chart.defaults.global.tooltips.titleMarginBottom = 16;
Chart.defaults.global.tooltips.bodyFontFamily = 'Roboto, sans-serif';
Chart.defaults.global.tooltips.bodyFontSize = 14;
Chart.defaults.global.tooltips.bodySpacing = 10;
Chart.defaults.global.tooltips.yPadding = 10;

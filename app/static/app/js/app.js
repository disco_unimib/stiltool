$(window).on('load resize', function () {
  fixAdminLTELayout();
});

$(function () {
  // if "multi-step" is in sidebar, add scrollbar (slim scroll)
  setTimeout(function () {
    const sideBarMenuHeight = $('.sidebar-menu.tree').height();

    $('.multiStepScrollbar').slimScroll({
      height: `calc(100vh - 100px - ${sideBarMenuHeight}px)`,
      size: '3px',
    });
  }, 500);

  // close sidebar control
  $('.control-sidebar .close').click(function () {
    $('body').removeClass('control-sidebar-open');
  });

  //Material Design Forms
  MDForms();

  $('.snackbar .dismiss').on('click', function () {
   dismissSnackbar()
  });

});

function fixAdminLTELayout (callback) {
  // fix adminLTE incompatibility between fixed layout and !expandOnHover sidebar
  // remove fixed from HTML
  // set height to content-wrapper
  // sidebar overflow is disabled -> use only if you have a short nav in sidebar

  if ($('body').hasClass('sde-fixed')) {
    // $("body").removeClass('sde-fixed');

    const headerHeight = $('.main-header').height();
    $('.content-wrapper').css('max-height', `calc(100vh - ${headerHeight}px`);

    setTimeout(function () {
      $('.content-wrapper').css('min-height', `calc(100vh - ${headerHeight}px`);
    }, 200);

    $('body, .wrapper').addClass('overflow-hidden');
  }

  if (typeof callback === 'function') {
    callback();
  }
}

function calcPageContentHeight () {
  const bodyHeight = '100vh';
  const headerHeight = $('.main-header').height();
  const contentPadding = $('.content').css('padding') && $('.content').css('padding').replace('px', '') * 2;
  const twoSideLayout = $('.twoSide-layout').length ? -contentPadding : 0;
  return `calc(${bodyHeight} - ${headerHeight + contentPadding + twoSideLayout}px)`;

}

function hex2rgb (hex) {
  return ['0x' + hex[ 1 ] + hex[ 2 ] | 0, '0x' + hex[ 3 ] + hex[ 4 ] | 0, '0x' + hex[ 5 ] + hex[ 6 ] | 0];
}

function makeUrl (url) {
  return $('<a />', { href: url, text: url, title: url, target: '_blank' });
}

function MDForms () {

  formOnLoad();

  $('.form-group.material').find('input, textarea, select').on('input', function () {
    resetGlobalError($(this).closest('form'));

    $(this).parent().removeClass('error');

    if ($(this).parent().hasClass('trailing-icon')) {
      if ($(this).val() !== '') {
        $('.clear').show();
      } else {
        $('.clear').hide();
      }
    }
  });

  $('.form-group.material').find('input, textarea, select').focus(function () {
    floatLabel($(this));
    $(this).parent().find('label').addClass('focus');

  });

  $('.form-group.material').find('input, textarea, select').focusout(function () {
    $(this).parent().find('label').removeClass('label-float focus');

    if ($(this).val() !== null && $(this).val() !== '') {
      $(this).parent().find('label').addClass('label-float');
    }
  });

  $('.form-group.material.input-radio input').on('change', function () {
    resetGlobalError($(this).closest('form'));
    const currentFormGroup = $(this).closest('.form-group');
    currentFormGroup.removeClass('error');
    const currentInputLabel = $(this).closest('label');
    $(currentFormGroup).find('label').removeClass('selected');

    if (this.checked) {
      $(currentInputLabel).addClass('selected');
    }
  });

  $('.form-group.material.input-checkbox input').on('change', function () {
    resetGlobalError($(this).closest('form'));
    $(this).closest('.form-group').removeClass('error');

    const currentInputLabel = $(this).closest('label');

    if (this.checked) {
      $(currentInputLabel).addClass('selected');
    } else {
      $(currentInputLabel).removeClass('selected');
    }
  });

  $('.form-group.material.trailing-icon .clear').on('click', function () {

    $(this).hide().closest('.form-group.material').find('input').val('').trigger('focusout');
  });

  // show/hide password
  $('.show-password').on('click', function () {
    const passwordInput = $(this).parent().find('input');
    if ($(passwordInput).attr('type') === 'password') {
      passwordInput.attr('type', 'text');
    } else {
      passwordInput.attr('type', 'password');
    }

    $(this).toggleClass('fa-eye-slash fa-eye');
  });

  function floatLabel (currentInput) {
    $(currentInput).closest('.form-group.material').find('label').addClass('label-float');
  }

  function formOnLoad () {

    //if input/select/textarea contains a value, set float label
    $.each($('.form-group.material').find('input, select, textarea'), function () {
      if ($(this).val() !== undefined && $(this).val() !== '') {
        floatLabel($(this));
      }

      try {
        setTimeout(function () {
          $(`input:-webkit-autofill`).each(function () {
            floatLabel($(this));
          });
        }, 200);
      } catch {
      }

    });

    // select radio/checkbox
    $.each($('input[type="radio"], input[type="checkbox"]'), function () {
      if (this.checked) {
        const currentInputLabel = $(this).closest('label');
        $(currentInputLabel).addClass('selected');
      }
    });

    // disable input form-group if input is disabled
    disableAllTextInput($('input[disabled=true]'));
    // disable input radio/checkbox if parent is disabled
    disableAllRadioInput($('.form-group.material.disabled'));

  }

}

function isValidForm (submitEvent) {
  const form = submitEvent.target;
  if (!form.checkValidity()) {
    // MDFormErrors.setSubmitButtonError( MDFormUtils.findSubmitButton( $( form ) ) );

    const formItems = form.elements;

    $.each(formItems, function (index, value) {
      if (value.willValidate && !value.validity.valid) {
        setErrorMessage($(value), 'Required');
      }
    });

    const $firstError = $(form).find($('.form-group.error')).first();
    // MDFormUtils.setFocus( $firstError );

    return false;
  }

  return true;
};

function setErrorMessage (item, message) {
  const $formGroup = $(item).closest('.form-group');
  let errorMessageDiv = $formGroup.find('.form-error-message');

  if (!errorMessageDiv.length) {
    errorMessageDiv = $('<div />', {
      class: 'form-error-message'
    });
    $formGroup.append(errorMessageDiv);
  }

  $formGroup.addClass('error');
  errorMessageDiv.html(message);

}

function removeErrorMessage (formGroup) {
  // const formGroup = $(item).closest('.form-group.material');
  const errorMessageDiv = formGroup.find('.form-error-message');

  $(formGroup).removeClass('error');
  errorMessageDiv.empty();
}

function resetGlobalError (form) {
  const submitButton = $(form).find('.btn.loading-animation');
  $(submitButton).removeClass('error');
  $('.form-error-message.global').hide();

}

function resetErrorsAndNotifications (form) {
  const formGroups = $(form).find('.form-group');
  removeErrorMessage(formGroups);
  resetGlobalError(form);
  dismissSnackbar();
}

function resetSubmitButton (button) {
  $(button).removeClass('loading').removeClass('disabled').prop('disabled', false);
  $(button).find('.progress').css('width', 0);
}

function clearAllRadioInput (formInstance) {

  const radio = formInstance.find('input[type="radio"]');
  $(radio).closest('label').removeClass('selected disabled');
  $(radio).prop('checked', false).prop('disabled', false);

}

function enableAllRadioInput (inputGroup) {
  const formGroup = $(inputGroup).closest('.form-group.material');
  formGroup.removeClass('disabled');
  formGroup.find('label').removeClass('disabled');
  formGroup.find('input').prop('disabled', false);
}

function disableAllTextInput (input) {
  $(input).closest('.form-group').addClass('disabled');
}

function disableAllRadioInput (formGroup) {
  if ($(formGroup).hasClass('input-radio') || $(formGroup).hasClass('input-checkbox')) {
    formGroup.find('label').addClass('disabled');
    formGroup.find('input').prop('disabled', true);
  }
}

function manageSnackbar (text) {
  const $snackbar = $('.snackbar');
  const $snackbarText = $snackbar.find('.text');
  $snackbarText.text(text);

  $snackbar.addClass('show');

  setTimeout(function () {
    $snackbar.removeClass('show');
  }, 10000);

}

function dismissSnackbar(){
  $('.snackbar').removeClass('show');
}

function setSlimScrollGsCol () {

  $(window).on('load resize', function () {

    if ($(document).width() >= 768) {

      $('.gs-col').slimScroll({
        height: calcPageContentHeight() - 10,
        size: '5px',
        color: '#979ca7',
      });

    } else {
      $('.gs-col').slimScroll({ destroy: true });
      $('.gs-col').attr('style', '');
    }
  });

}

function setTooltipScore () {

  $('[data-toggle="popover"]').each(function () {
    if ($(this).attr('data-scoreName')) {
      const scoreName = $(this).attr('data-scoreName').replace(/[\. ]+/g, '-').toLowerCase().replace('-score', '');
      $(this).popover({
        html: true,
        trigger: 'focus, hover',
        container: 'body',
        placement: 'bottom',
        content: $(`#${scoreName}`).html(),
        template: '<div class="popover scoreDescription" role="tooltip"><div class="popover-content"></div></div>'
      });
    }

  });

  $('.fa-question-circle.info').on('click mouseover', function () {
    MathJax.typeset();
  });

  function getTooltipScore (scoreName) {
    let scoreDesc = '';
    switch (scoreName.toLowerCase()) {
      case 'precision':
        scoreDesc = 'Precision = (# perfect annotations) / (# submitted annotations)';
        break;
      case 'recall':
        scoreDesc = 'Recall = (# perfect annotations) / (# ground truth annotations)';
        break;
      case 'f1':
        scoreDesc = 'F1 Score =(2 * Precision * Recall) / (Precision + Recall)';
        break;
      case 'ah':
      case 'ah score':
        scoreDesc = 'AH-Score = (1 * (# perfect annotations) + 0.5 * (# okay annotations) - 1 * (# wrong annotations)) / (# target columns)';
        break;
      case 'ap':
      case 'ap score':
        scoreDesc = 'AP-Score = (# perfect annotations) / (# total annotated classes)';
        break;
    }

    return scoreDesc;
  }
}

function updateMathContent (s) {
  var math = MathJax.Hub.getAllJax('mathdiv')[ 0 ];
  MathJax.Hub.Queue(['Text', math, s]);
}

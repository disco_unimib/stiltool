from stiltool.celery import app
from django.contrib.auth.models import User
from app.evaluation.score.ahap_evaluator import CTAAHAPEvaluator
from app.evaluation.score.cpa_evaluator import CPAEvaluator
from app.evaluation.score.cea_evaluator import CEAEvaluator
from app.evaluation.score.cta_prf1_evaluator import CTAPRF1Evaluator
from app.evaluation.score.appr_prf1_evaluator import CTAApprPRF1Evaluator
from .models import GoldenStandard, UserAnnotation, TasksEnum, GSModeEnum
from django.core.files.uploadedfile import SimpleUploadedFile
from .publisher.internal import Internal

import stiltool.settings

import json
from io import BytesIO
import zipfile
import datetime

def zip_it(data):
    content = json.dumps(data, indent=4)

    in_memory = BytesIO()
    with zipfile.ZipFile(in_memory, "a", zipfile.ZIP_DEFLATED, False) as zf:
        zf.writestr("data", content)

        for f in zf.filelist:
            f.create_system = 0

    in_memory.seek(0)

    return SimpleUploadedFile("name", in_memory.read())


@app.task(name="load_annotation")
def load_annotation(ann_id, task_choice):
    ann = UserAnnotation.objects.get(id=ann_id)
    if task_choice == TasksEnum.CEA.value:
        ev = CEAEvaluator(ann)
    elif task_choice == TasksEnum.CPA.value:
        ev = CPAEvaluator(ann)
    else:
        if ann.gs.tasks_metadata["cta_format"] == "prf1":
            ev = CTAPRF1Evaluator(ann)
        elif ann.gs.tasks_metadata["cta_format"] == "aprf":
            ev = CTAApprPRF1Evaluator(ann)
        else:
            ev = CTAAHAPEvaluator(ann)

    ev.compute()
    ann.score = ev.score
    ann.RWNa_statistics = ev.general_statistics
    if task_choice != TasksEnum.CEA.value:
        ann.annotation_statistics = ev.annotation_statistics
    else:
        ann.annotation_statistics = []

    ann.loaded_annotations = {
        "annotated": ev.annotated,
        "targets": ev.targets
    }

    if stiltool.settings.CHALLENGE_MODE and ann.gs.settings["mode"] == GSModeEnum.score.value:
        ann.annotations = None
    else:
        ann.annotations = zip_it(ev.evaluated_annotations)

    ann.todo = False
    ann.save()

    Internal.annotation().notify_job_complete(ann.id)

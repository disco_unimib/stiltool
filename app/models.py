from django.db import models
from djongo.models.json import JSONField
from django.utils.timezone import now
from django.contrib.auth.models import User
from django.dispatch import receiver

from app.locations import Locations

import os
import json

from enum import Enum
import zipfile
from io import BytesIO

import datetime

import stiltool.settings


class TasksEnum(Enum):
    CEA = "CEA"
    CPA = "CPA"
    CTA = "CTA"


class GSScopeEnum(Enum):
    public = "public"
    private = "private"


class GSModeEnum(Enum):
    info = "info"
    score = "score"


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    avatar = models.ImageField(upload_to=Locations.get_user_location, default='default_avatar.png')


class GoldenStandard(models.Model):
    class Meta:
        db_table = 'stiltool_gs'

    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    settings = JSONField()

    _cea = models.FileField(upload_to=Locations.get_cea_location)
    _cpa = models.FileField(upload_to=Locations.get_cpa_location)
    _cta = models.FileField(upload_to=Locations.get_cta_location)

    _ancestors = models.FileField(upload_to=Locations.get_ancestors_location)
    _descendents = models.FileField(upload_to=Locations.get_descendents_location)

    tasks_metadata = JSONField()
    task_list = JSONField(default=[])

    # TODO: This is not used... DEPRECATE IT!!!
    tables = models.FileField(upload_to=Locations.get_gt_tables_location)

    max_daily_uploads = models.IntegerField(default=-1)

    pub_date = models.DateTimeField(default=now)

    @property
    def cea(self):
        if self._cea:
            data = json.loads(self._cea.read())
            self._cea.seek(0)
            return data

        return None

    @cea.setter
    def cea(self, value):
        if value is not None and "CEA" not in self.task_list:
            self.task_list.append("cea")
        self._cea = value

    @property
    def cpa(self):
        if self._cpa:
            data = json.loads(self._cpa.read())
            self._cpa.seek(0)
            return data

        return None

    @cpa.setter
    def cpa(self, value):
        if value is not None and "CPA" not in self.task_list:
            self.task_list.append("cpa")
        self._cpa = value

    @property
    def cta(self):
        if self._cta:
            data = json.loads(self._cta.read())
            self._cta.seek(0)
            return data

        return None

    @cta.setter
    def cta(self, value):
        if value is not None and "CTA" not in self.task_list:
            self.task_list.append("cta")
        self._cta = value

    @property
    def ancestors(self):
        if self._ancestors:
            data = json.loads(self._ancestors.read())
            self._ancestors.seek(0)
            return data

        return None

    @ancestors.setter
    def ancestors(self, value):
        self._ancestors = value

    @property
    def descendents(self):
        if self._descendents:
            data = json.loads(self._descendents.read())
            self._descendents.seek(0)
            return data

        return None

    @descendents.setter
    def descendents(self, value):
        self._descendents = value

    def is_simple_cta(self):
        if self.cta is None:
            return False

        return self.tasks_metadata["cta_format"] == "prf1"

    def is_approximate_cta(self):
        if self.cta is None:
            return False

        return self.tasks_metadata["cta_format"] == "aprf"

    def get_task(self, task_enum):
        assert (isinstance(task_enum, TasksEnum))

        task_map = {
            TasksEnum.CTA: lambda: self.cta,
            TasksEnum.CPA: lambda: self.cpa,
            TasksEnum.CEA: lambda: self.cea,
        }

        return task_map.get(task_enum, lambda: None)()

    def get_tasks_list(self):
        result = []
        for e in TasksEnum:
            if e.value.lower() in self.task_list:
                result.append(e)

        return result


class UserAnnotation(models.Model):
    class Meta:
        db_table = 'stiltool_annotation'

    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    gs = models.ForeignKey(GoldenStandard, on_delete=models.CASCADE)

    _task_type = models.CharField(max_length=10, choices=[
        (task.name, task.value) for task in TasksEnum
    ], default=TasksEnum.CTA.value)

    score = JSONField()
    RWNa_statistics = JSONField()
    annotation_statistics = JSONField()

    loaded_annotations = JSONField()
    _annotations = models.FileField(upload_to=Locations.get_anns_location)
    todo = models.BooleanField(default=True)

    added_date = models.DateTimeField(default=now)

    @property
    def annotations(self):
        if self._annotations:
            data = ""

            zipcontent = BytesIO(self._annotations.read())
            with zipfile.ZipFile(zipcontent, 'r') as zf:
                content = zf.read("data")
                data = json.loads(content.decode("utf-8"))

            self._annotations.seek(0)
            return data

    @annotations.setter
    def annotations(self, value):
        self._annotations = value

    @property
    def task_type(self):
        task_map = {
            "CTA": TasksEnum.CTA,
            "CPA": TasksEnum.CPA,
            "CEA": TasksEnum.CEA,
        }

        task = task_map.get(self._task_type, None)

        assert (task is not None)  # Programming error
        return task

    @task_type.setter
    def task_type(self, task_enum):
        assert (isinstance(task_enum, TasksEnum))

        self._task_type = task_enum.value


class Board(models.Model):
    class Meta:
        db_table = 'stiltool_board'

    title = models.CharField(max_length=255)
    message = models.TextField()
    pub_date = models.DateTimeField(default=now)
    pinned = models.BooleanField(default=False)


# Signals ============================================================

@receiver(models.signals.post_delete, sender=UserAnnotation)
def remove_file_on_delete(sender, instance, **kwargs):
    if instance._annotations:
        path = os.path.join(stiltool.settings.MEDIA_ROOT, instance._annotations.path)
        if os.path.isfile(path):
            os.remove(path)


@receiver(models.signals.pre_save, sender=UserAnnotation)
def remove_file_on_change(sender, instance, **kwargs):
    if not instance.pk:
        return False

    try:
        old_file = UserAnnotation.objects.get(id=instance.id)._annotations
    except UserAnnotation.DoesNotExist:
        return False

    new_file = instance._annotations
    if not old_file == new_file:
        path = os.path.join(stiltool.settings.MEDIA_ROOT, old_file.path)
        if os.path.isfile(path):
            os.remove(path)


@receiver(models.signals.post_delete, sender=GoldenStandard)
def remove_file_on_delete_gs(sender, instance, **kwargs):
    instance_files = [
        instance._cea,
        instance._cpa,
        instance._cta,
        instance._ancestors,
        instance._descendents
    ]

    for c in instance_files:
        if c:
            path = os.path.join(stiltool.settings.MEDIA_ROOT, c.path)
            if os.path.isfile(path):
                os.remove(path)


@receiver(models.signals.pre_save, sender=GoldenStandard)
def remove_file_on_change_gs(sender, instance, **kwargs):
    if not instance.pk:
        return False

    try:
        gs = GoldenStandard.objects.get(id=instance.id)
    except GoldenStandard.DoesNotExist:
        return False

    gs_map = [
        (gs._cea, instance._cea),
        (gs._cpa, instance._cpa),
        (gs._cta, instance._cta),
        (gs._ancestors, instance._ancestors),
        (gs._descendents, instance._descendents),
    ]

    for old_file, new_file in gs_map:
        if not old_file == new_file:
            path = os.path.join(stiltool.settings.MEDIA_ROOT, old_file.path)
            if os.path.isfile(path):
                os.remove(path)

    return True

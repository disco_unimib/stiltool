import datetime
import json
import urllib

import stiltool.settings
from app.evaluation.score.ahap_evaluator import CTAAHAPEvaluator
from app.evaluation.score.cea_evaluator import CEAEvaluator
from app.evaluation.score.cpa_evaluator import CPAEvaluator
from app.evaluation.score.cta_prf1_evaluator import CTAPRF1Evaluator
from app.tokengen import account_activation_token

from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.models import User
from django.core.mail import EmailMessage
from django.http import JsonResponse, QueryDict
from django.shortcuts import redirect, render
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from django.contrib.auth.decorators import user_passes_test

from .forms import (ChangeGsSettings, ImageUploadForm, LoginForm, MessageForm,
                    RegistrationForm, UploadAnnotationForm, UploadGSForm)
from .models import (Board, GoldenStandard, GSModeEnum, GSScopeEnum, TasksEnum,
                     UserAnnotation, UserProfile)
from .tasks import load_annotation


def index(request):
    context = {
        "challenge": stiltool.settings.CHALLENGE_MODE
    }
    return render(request, 'app/index.html', context)


def login_user(request):
    if request.method == 'POST':
        form = LoginForm(data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                next_url = request.POST.get('next')

                if next_url:
                    # return redirect(next_url)
                    redirect = next_url
                else:
                    if stiltool.settings.CHALLENGE_MODE:
                        redirect = reverse("board")
                        # return redirect("leaderboard")
                    else:
                        redirect = reverse("annotations")
                        # return redirect("annotations")

                return JsonResponse({"redirect": redirect})
            else:
                print("1")
                print("Bad login")
                # This will never happen (probably)
                return JsonResponse({}, status=400)
        else:
            print("2")
            print("Bad login")
            errors = {}
            errors.update(form.errors)
            data = errors

            return JsonResponse(data, status=400)

        # return render(request, "app/login.html", {"login_form": form, "challenge": stiltool.settings.CHALLENGE_MODE})

    context = {
        "login_form": LoginForm(),
        "challenge": stiltool.settings.CHALLENGE_MODE,
        "show_demo": User.objects.filter(username="demo").count() > 0 and not stiltool.settings.CHALLENGE_MODE
    }
    return render(request, "app/login.html", context)


def logout_user(request):
    logout(request)

    if stiltool.settings.CHALLENGE_MODE:
        return redirect("leaderboard")
    else:
        return redirect("annotations")


def register(request):
    if stiltool.settings.PORT != '443' and stiltool.settings.PORT != '80':
        host = "%s:%s" % (stiltool.settings.HOST, stiltool.settings.PORT)
    else:
        host = stiltool.settings.HOST

    if request.method == "POST":
        form = RegistrationForm(data=request.POST)
        image_form = ImageUploadForm(request.POST, request.FILES)

        if form.is_valid() and image_form.is_valid():
            user = form.save(commit=False)
            user.is_active = False
            user.save()

            avatar = image_form.cleaned_data["avatar"]

            if avatar is not None:
                UserProfile(
                    user=user,
                    avatar=avatar
                ).save()
            else:
                UserProfile(
                    user=user,
                ).save()

            if stiltool.settings.EMAIL_HOST_USER != '' and "@" in stiltool.settings.EMAIL_HOST_USER:
                mail_subject = 'Stiltool Account Activation'
                message = render_to_string('app/email_activation.html', {
                    'user': user,
                    'domain': host,
                    'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                    'token': account_activation_token.make_token(user),
                })
                to_email = form.cleaned_data.get('email')
                email = EmailMessage(
                    mail_subject, message, to=[to_email]
                )
                email.send()
            else:
                user.is_active = True
                user.save()

            redirect = reverse("annotations")
            return JsonResponse({"redirect": redirect})
        else:
            print("register invalid")
            errors = {}
            if not form.is_valid():
                errors.update(form.errors)

            if not image_form.is_valid():
                errors.update(image_form.errors)

            data = errors
            print(data)
            return JsonResponse(data, status=400)

    registration_form = RegistrationForm()
    image_form = ImageUploadForm()
    return render(request, "app/register.html", {
        "registration_form": registration_form,
        "image_form": image_form,
        "smtp": json.dumps(stiltool.settings.EMAIL_HOST_USER != '' and "@" in stiltool.settings.EMAIL_HOST_USER),
        "challenge": stiltool.settings.CHALLENGE_MODE
    })


def account_activation(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()

        messages.add_message(request, messages.SUCCESS,
                             'Registration complete: you can now login with your credentials')
    else:
        messages.add_message(request, messages.ERROR, 'Token is invalid or is expired')
        # login(request, user)
        # return redirect('home')

    return render(request, "app/login.html", {
        "login_form": LoginForm(),
        "challenge": stiltool.settings.CHALLENGE_MODE}
                  )


def profile(request):
    username = request.GET.get("user", None)
    if username is None:
        user = request.user
    else:
        user = User.objects.filter(username=username).first()

        if user is None:
            if stiltool.settings.CHALLENGE_MODE:
                return redirect("leaderboard")
            else:
                return redirect("annotations")

    if request.user == user and request.user.is_superuser:
        user_list = User.objects.exclude(is_superuser=True).order_by("username")

        context = {
            "user": user,
            "user_list": user_list,
            "challenge": stiltool.settings.CHALLENGE_MODE
        }
    else:
        context = {
            "user": user,
            "challenge": stiltool.settings.CHALLENGE_MODE
        }

    if request.method == "POST":
        if request.user.is_superuser:
            user_list = User.objects.exclude(is_superuser=True).order_by("username")

            for user in user_list:
                if user.username in request.POST.keys():
                    user.is_staff = True
                else:
                    user.is_staff = False

                user.save()

            return render(request, 'app/user_profile.html', context)
        else:
            return None
    else:
        return render(request, 'app/user_profile.html', context)


# @permission_required('is_superuser', raise_exception=True)
@user_passes_test(lambda u: u.is_staff)
def admin_console(request):
    user_list = User.objects.exclude(is_superuser=True).order_by("username")

    users_annotations = []

    for ann in UserAnnotation.objects.order_by("-added_date"):
        users_annotations.append(
            {
                "id": ann.id,
                "user": ann.owner.username,
                "gs": ann.gs.name,
                "task": ann._task_type,
                "added_date": ann.added_date,
                "todo": ann.todo
            }
        )

    context = {
        "user_list": user_list,
        "annotations": users_annotations,
        "challenge": stiltool.settings.CHALLENGE_MODE,
    }
    return render(request, 'app/admin_console.html', context)


def edit_user_status(request):
    if request.method == 'PUT' and request.is_ajax():
        put = QueryDict(request.body)
        username = put.get("username", None)
        user = User.objects.filter(username=username).first()

        if user:
            user.is_staff = not user.is_staff
            user.save()
            return JsonResponse({"Status": "OK"})
        else:
            return JsonResponse({"Status": "Bad request"}, 400)


def board(request):
    if request.method == "POST" and (request.user.is_superuser or request.user.is_staff):
        main_form = MessageForm(prefix="main", data=request.POST)
        message_form = MessageForm(prefix="board", data=request.POST)

        if main_form.is_valid():
            pinned_msg = Board.objects.filter(pinned=True)
            if pinned_msg.count() > 0:
                pin = pinned_msg[0]
                pin.title = main_form.cleaned_data["title"]
                pin.message = main_form.cleaned_data["msg"]
                pin.pinned = True
                pin.save()
            else:
                Board(
                    title=main_form.cleaned_data["title"],
                    message=main_form.cleaned_data["msg"],
                    pinned=True
                ).save()

        elif message_form.is_valid():
            Board(
                title=message_form.cleaned_data["title"],
                message=message_form.cleaned_data["msg"],
                pinned=False
            ).save()

        return redirect('board')

    elif request.method == 'DELETE' and (request.user.is_superuser or request.user.is_staff):
        put = QueryDict(request.body)
        msg_id = put.get('id')

        message = Board.objects.filter(id=msg_id)
        if message.count() > 0:
            message.delete()
            return JsonResponse({"status": "deleted"})

        return JsonResponse({"status": "bad id"})

    messages = Board.objects.filter(pinned=False).order_by("-pub_date")
    main = Board.objects.filter(pinned=True)
    if main.count() > 0:
        main = main[0]
    else:
        main = None

    context = {
        "main": main,
        "messages": messages,
        "message_form": MessageForm(prefix="board"),
        "challenge": stiltool.settings.CHALLENGE_MODE
    }
    return render(request, 'app/board.html', context)


@login_required
def annotations(request):
    # TODO: Extract method: this is an utility function
    def redirect_params(url, params=None):
        response = redirect(url)
        if params:
            query_string = urllib.parse.urlencode(params)
            response['Location'] += '?' + query_string

        return response

    gs_objects = [
        gs
        for gs in GoldenStandard.objects.all().order_by("-pub_date")
        if (gs.owner == request.user or gs.settings[
            "scope"] == GSScopeEnum.public.value) and UserAnnotation.objects.filter(owner=request.user,
                                                                                    gs=gs).count() > 0
    ]

    gs_param = request.GET.get("gs", None)
    task_param = request.GET.get("task", None)

    if gs_param is None:
        print("gs none", gs_objects)
        if len(gs_objects) > 0:
            tasks = [
                task
                for task in gs_objects[0].get_tasks_list()
                if
                UserAnnotation.objects.filter(owner=request.user, gs=gs_objects[0], _task_type=task.value,
                                              todo=False).count() > 0
            ]
            if len(tasks) > 0:
                return redirect_params('annotations', {
                    "gs": gs_objects[0].name,
                    "task": tasks[0].value,
                })
            else:
                print([
                    task
                    for task in gs_objects[0].get_tasks_list()
                ])

        # return redirect('index')
        context = {
            'gs_list': [],
            'tasks': {},
            'history': [],
            'chart': [],
            'has_no_data': True,
            "challenge": stiltool.settings.CHALLENGE_MODE
        }

        return render(request, 'app/annotations.html', context)

    found = GoldenStandard.objects.filter(name=gs_param).count() > 0
    gs = None
    if found:
        gs = GoldenStandard.objects.filter(name=gs_param).first()
    else:
        print("gs wrong")
        if len(gs_objects) > 0:
            return redirect_params('annotations', {
                "gs": gs_objects[0].name,
                "task": TasksEnum.CEA.value,
            })

        return redirect('index')

    if task_param is None:
        print("task none")
        tasks = [
            task
            for task in gs.get_tasks_list()
            if UserAnnotation.objects.filter(owner=request.user, gs=gs, _task_type=task.value).count() > 0
        ]
        if len(tasks) > 0:
            return redirect_params('annotations', {
                "gs": gs_param,
                "task": tasks[0].value,
            })

        return redirect('index')
    elif task_param not in [t.value for t in gs.get_tasks_list()]:
        print(gs.get_tasks_list())
        return redirect_params('annotations', {
            "gs": gs_param,
            "task": gs.get_tasks_list()[0].value,
        })

    gs_list = {
        gs_all.name: {
            "availability": gs_all.settings["scope"],
            "mode": gs_all.settings["mode"]
        }
        for gs_all in gs_objects
        if UserAnnotation.objects.filter(owner=request.user, gs=gs_all).count() > 0
    }
    tasks = {
        task.value: [
            {
                "id": ann.id,
                "added_date": ann.added_date,
                "score": ann.score.values,
                "gs_mode": ann.gs.settings["mode"],
                "annotations": {
                    "ann_loaded": ann.loaded_annotations,
                    "ann_loaded_percentage": (
                            100 * ann.loaded_annotations['annotated'] / ann.loaded_annotations['targets'])
                }
            }
            for ann in UserAnnotation.objects.filter(owner=request.user, gs=gs, _task_type=task.value, todo=False)
        ]
        for task in gs.get_tasks_list()
        if UserAnnotation.objects.filter(owner=request.user, gs=gs, _task_type=task.value).count() > 0
    }

    if UserAnnotation.objects.filter(owner=request.user, gs=gs, _task_type=task_param, todo=False).count() > 0:
        score_header = [
            score_val["label"]
            for score_val in
            UserAnnotation.objects.filter(owner=request.user, gs=gs, _task_type=task_param,
                                          todo=False).first().score.values()
        ]
    else:
        score_header = []

    has_no_data = len(gs_list) == 0

    pending = {
        task.value: [
            {
                "id": ann.id,
                "added_date": ann.added_date,
                "gs_mode": ann.gs.settings["mode"],
            }
            for ann in UserAnnotation.objects.filter(owner=request.user, gs=gs, _task_type=task.value, todo=True)
        ]
        for task in gs.get_tasks_list()
        if UserAnnotation.objects.filter(owner=request.user, gs=gs, _task_type=task.value, todo=True).count() > 0
    }

    annotations = UserAnnotation.objects.filter(owner=request.user, gs=gs, _task_type=task_param, todo=False)

    # TODO: use enums
    if task_param == "CPA" or task_param == "CEA" or gs.is_simple_cta():
        chart = {
            'xAxisLabels': [
                ann.added_date.strftime('%d/%m/%Y - %H:%M:%S')
                for ann in annotations
            ],
            'datasets': {
                'Precision': [

                    ann.score["P"]["value"]
                    for ann in annotations
                ],
                'Recall': [

                    ann.score["R"]["value"]
                    for ann in annotations
                ],
                'F1': [
                    ann.score["F1"]["value"]
                    for ann in annotations
                ]
            }
        }
    else:
        if gs.is_approximate_cta():
            chart = {
                'xAxisLabels': [
                    ann.added_date.strftime('%d/%m/%Y - %H:%M:%S')
                    for ann in annotations
                ],
                'datasets': {
                    'Appr. Precision': [
                        ann.score["AP"]["value"]
                        for ann in annotations
                    ],
                    'Appr. Recall': [

                        ann.score["AR"]["value"]
                        for ann in annotations
                    ],
                    'Appr. F1': [
                        ann.score["AF1"]["value"]
                        for ann in annotations
                    ]
                }
            }
        else:
            chart = {
                'xAxisLabels': [
                    ann.added_date.strftime('%d/%m/%Y - %H:%M:%S')
                    for ann in annotations
                ],
                'datasets': {
                    'AH': [
                        ann.score["AH"]["value"]
                        for ann in annotations
                    ],
                    'AP': [
                        ann.score["AP"]["value"]
                        for ann in annotations
                    ]
                }
            }

    context = {
        'gs_list': gs_list,
        'tasks': tasks,
        'score_header': score_header,
        'pending': pending,
        'chart': json.dumps(chart),
        'has_no_data': has_no_data,
        "challenge": stiltool.settings.CHALLENGE_MODE
    }

    return render(request, 'app/annotations.html', context)


@login_required
def delete_annotation(request):
    if request.is_ajax() and request.method == 'DELETE':
        delete = QueryDict(request.body)
        annId = delete.get("annId", None)

        if annId is not None:
            ann = UserAnnotation.objects.filter(id=annId)
            if ann.count() > 0:
                if request.user == ann[0].owner:
                    ann[0].delete()
                else:
                    return JsonResponse({"Status": "Bad annotation id"}, 400)
            else:
                return JsonResponse({"Status": "Bad annotation id"}, 400);

            return JsonResponse({"Status": "OK"})
        else:
            return JsonResponse({"Status": "Bad request"}, 400)

    return redirect('index')


def leaderboard(request):
    # TODO: Extract method: this is an utility function
    def redirect_params(url, params=None):
        response = redirect(url)
        if params:
            query_string = urllib.parse.urlencode(params)
            response['Location'] += '?' + query_string

        return response

    def create_ranking(gs_obj, task):
        partecipant = []
        for user in User.objects.all():
            anns = UserAnnotation.objects.filter(owner=user, gs=gs_obj, _task_type=task, todo=False)

            anns = sorted(anns, key=lambda item: item.score[list(item.score.keys())[0]]["value"], reverse=True)
            if len(anns) > 0:
                last = anns[0]
                anns_secondary = list(filter(
                    lambda item: item.score[list(item.score.keys())[0]]["value"] ==
                                 last.score[list(item.score.keys())[0]]["value"],
                    anns))
                anns_secondary = sorted(anns_secondary,
                                        key=lambda item: item.score[list(item.score.keys())[1]]["value"],
                                        reverse=True)
                last = anns_secondary[0]

                partecipant.append((last, len(anns)))

        # TODO: Use enum
        header = lambda prf1: ["F1 Score", "Precision"] if prf1 else ["AH Score", "AP Score"]

        def header(score_type):
            if score_type == "PRF1":
                return ["F1 Score", "Precision"]
            elif score_type == "AHAP":
                return ["AH Score", "AP Score"]
            else:
                return ["Appr. F1 Score", "Appr. Precision"]

        if task == "CPA" or task == "CEA" or gs_obj.is_simple_cta():
            score = lambda ann: [
                ann.score["F1"]["value"],
                ann.score["P"]["value"],
            ]
        else:
            if gs_obj.is_approximate_cta():
                score = lambda ann: [
                    ann.score["AF1"]["value"],
                    ann.score["AP"]["value"],
                ]
            else:
                score = lambda ann: [
                    ann.score["AH"]["value"],
                    ann.score["AP"]["value"],
                ]

        data = [
            {
                "partecipant": {
                    "name": ann.owner.username,
                    "icon": ann.owner.userprofile.avatar,
                },
                "score": score(ann),
                "entries": entries,
                "last_sub": ann.added_date
            }
            for ann, entries in partecipant
        ]

        data = sorted(data, key=lambda item: item["score"][0], reverse=True)

        ttype = "PRF1"
        if task == "CTA":
            if gs_obj.is_simple_cta():
                ttype = "PRF1"
            elif gs_obj.is_approximate_cta():
                ttype = "APRF"
            else:
                ttype = "AHAP"
        return {
            "header": header(ttype),
            "data": data
        }

    gs_objects = [
        gs
        for gs in GoldenStandard.objects.all().order_by("-pub_date")
        if gs.settings["scope"] == GSScopeEnum.public.value
    ]

    gs_param = request.GET.get("gs", None)
    task_param = request.GET.get("task", None)

    if gs_param is None:
        print("gs none")
        if len(gs_objects) > 0:
            tasks = gs_objects[0].get_tasks_list()
            if len(tasks) > 0:
                return redirect_params('leaderboard', {
                    "gs": gs_objects[0].name,
                    "task": tasks[0].value,
                })
            else:
                pass
                # Impossible

        # return redirect('index')
        context = {
            'gs_list': [],
            'tasks': {},
            'has_no_data': True,
            'has_no_data_gs': True,
            "challenge": stiltool.settings.CHALLENGE_MODE
        }

        return render(request, 'app/leaderboard.html', context)

    found = GoldenStandard.objects.filter(name=gs_param).count() > 0
    gs = None
    if found:
        gs = GoldenStandard.objects.filter(name=gs_param).first()
    else:
        print("gs wrong")
        if len(gs_objects) > 0:
            return redirect_params('leaderboard', {
                "gs": gs_objects[0].name,
                "task": TasksEnum.CEA.value,
            })

        return redirect('index')

    if task_param is None:
        print("task none")
        tasks = gs.get_tasks_list()
        if len(tasks) > 0:
            return redirect_params('leaderboard', {
                "gs": gs_param,
                "task": tasks[0].value,
            })

        return redirect('index')

    gs_list = [
        gs_all.name
        for gs_all in gs_objects
    ]
    tasks = {
        task.value: create_ranking(gs, task.value)
        for task in gs.get_tasks_list()
    }

    has_no_data = len(gs_list) == 0

    has_no_data_gs = {
        task: len(tasks[task]["data"]) == 0
        for task in tasks
    }
    print(has_no_data_gs)

    context = {
        # 'data': data,
        'gs_list': gs_list,
        'tasks': tasks,
        'has_no_data': has_no_data,
        'has_no_data_gs': has_no_data_gs,
        "challenge": stiltool.settings.CHALLENGE_MODE
    }

    return render(request, 'app/leaderboard.html', context)


@login_required
def evaluation(request, id):
    def make_main_chart(type, data):
        if type == "PRF1":
            labels = ["Right", "Wrong", "Not Annotated"]
            datatable = [data["right"], data["wrong"], data["na"]]

            return {
                "label": labels,
                "datatable": datatable
            }
        elif type == "AHAP":
            return {
                "ah_max": data["ah_max"],
                "ah_min": data["ah_min"],
                "ah_curr": data["ah_curr"],
            }
        else:
            labels = ["Right", "Okay", "Wrong", "Not Annotated"]
            datatable = [data["right"], data["ok"], data["wrong"], data["na"]]

            return {
                "label": labels,
                "datatable": datatable
            }

    def make_stackedbarJS(type, cta_simple, annotations):
        sorted_annotation = list(filter(
            lambda item: item["wrong"] + item["na"] > 0,
            sorted(annotations,
                   key=lambda item: (item["wrong"] + item["na"]),
                   reverse=True
                   )))[0:10]

        labels = [
            item["annotation"]
            for item in sorted_annotation
        ]

        if type == "PRF1":
            datasets = [
                {
                    'label': 'Wrong',
                    'stack': 'Errors',
                    'data': [
                        item["wrong"]
                        for item in sorted_annotation
                    ]
                },
                {
                    'label': 'Not Annotated',
                    'stack': 'Errors',
                    'data': [
                        item["na"]
                        for item in sorted_annotation
                    ]
                },
                {
                    'label': 'Right',
                    'stack': 'Correct',
                    'hidden': 'true',

                    'data': [
                        item["right"]
                        for item in sorted_annotation
                    ]
                },
            ]
        elif type == "AHAP":
            datasets = [
                {
                    'label': 'Wrong',
                    'stack': 'Errors',
                    'data': [
                        item["wrong"]
                        for item in sorted_annotation
                    ]
                },
                {
                    'label': 'Missing',
                    'stack': 'Errors',
                    'data': [
                        item["na"]
                        for item in sorted_annotation
                    ]
                },
                {
                    'label': 'Perfect',
                    'stack': 'Correct',
                    'hidden': 'true',
                    'data': [
                        item["perfect"]
                        for item in sorted_annotation
                    ]
                },
                {
                    'label': 'Okay',
                    'stack': 'Correct',
                    'hidden': 'true',
                    'data': [
                        item["okay"]
                        for item in sorted_annotation
                    ]
                }
            ]
        else:
            datasets = [
                {
                    'label': 'Wrong',
                    'stack': 'Errors',
                    'data': [
                        item["wrong"]
                        for item in sorted_annotation
                    ]
                },
                {
                    'label': 'Not Annotated',
                    'stack': 'Errors',
                    'data': [
                        item["na"]
                        for item in sorted_annotation
                    ]
                },
                {
                    'label': 'Okay',
                    'stack': 'Correct',
                    'hidden': 'true',

                    'data': [
                        item["ok"]
                        for item in sorted_annotation
                    ]
                },
                {
                    'label': 'Right',
                    'stack': 'Correct',
                    'hidden': 'true',

                    'data': [
                        item["right"]
                        for item in sorted_annotation
                    ]
                },
            ]

        return {
            "label": labels,
            "datasets": datasets
        }

    annotation = UserAnnotation.objects.filter(id=id, owner=request.user)
    if not (annotation.count() > 0 and annotation[0].gs.settings["mode"] == GSModeEnum.info.value):
        return redirect('annotations')

    annotation = annotation[0]
    if annotation.task_type == TasksEnum.CEA or \
            annotation.task_type == TasksEnum.CPA or \
            annotation.task_type == TasksEnum.CTA and annotation.gs.is_simple_cta():
        ttype = "PRF1"
    elif annotation.gs.is_approximate_cta():
        ttype = "APRF"
    else:
        ttype = "AHAP"

    completeness = annotation.RWNa_statistics
    annotation_focus = annotation.annotation_statistics

    main_chart = make_main_chart(ttype, completeness)
    secondary_chart = dict()

    if annotation.task_type != TasksEnum.CEA:
        secondary_chart = make_stackedbarJS(ttype, annotation.gs.is_simple_cta(), annotation_focus)

        print(secondary_chart, annotation_focus)

    score = [
        {
            "label": sitem["label"],
            "value": sitem["value"]
        }
        for sk, sitem in annotation.score.items()
    ]

    has_table_data = annotation.annotations is not None

    context = {
        "type": ttype,
        "task_type": annotation.task_type.value,
        "annotation": annotation,
        "loaded_annotations": {
            "ann_loaded": annotation.loaded_annotations,
            "ann_loaded_percentage": (
                    100 * annotation.loaded_annotations['annotated'] / annotation.loaded_annotations['targets'])
        },
        "score": score,
        "main_chart": main_chart,
        "secondary_chart": secondary_chart,
        "challenge": stiltool.settings.CHALLENGE_MODE,
        "has_table_data": has_table_data
    }
    return render(request, 'app/evaluation.html', context)


@login_required
def load(request):
    ann_form = UploadAnnotationForm(request.user, prefix="annotation_form")

    if request.method == 'POST' and request.is_ajax():
        ann_form = UploadAnnotationForm(request.user, request.POST, request.FILES, prefix="annotation_form")

        if ann_form.is_valid():
            # TODO: For now I manually save the annotation
            ann_file = ann_form.cleaned_data.get('annotation_file')
            gs_choice = ann_form.cleaned_data.get('gs_choice')
            task_choice = ann_form.cleaned_data.get('task_choice')

            # TODO: Warning code duplication
            try:
                gs = GoldenStandard.objects.filter(name=gs_choice)
            except GoldenStandard.DoesNotExist:
                gs = []

            gs = [
                g
                for g in gs
                if g.owner == request.user or g.settings["scope"] == GSScopeEnum.public.value
            ]

            if len(gs) == 0 or len(gs) > 1:  # This will not happen if you use the GUI
                return redirect('index')

            gs = gs[0]
            # TODO: Will this lead to nasty bugs because is not timezone aware??
            today = datetime.date.today()
            today_min = datetime.datetime.combine(today, datetime.time.min)
            today_max = datetime.datetime.combine(today, datetime.time.max)
            if stiltool.settings.CHALLENGE_MODE:
                ann_uploaded = UserAnnotation.objects.filter(
                    owner=request.user,
                    gs=gs,
                    _task_type=task_choice.value,
                    added_date__range=(today_min, today_max),
                ).count()

                if gs.max_daily_uploads != 0 and gs.max_daily_uploads - ann_uploaded == 0:
                    return JsonResponse({"status": "not enough annotations left"}, status=400)

            ann = UserAnnotation(
                owner=request.user,
                gs=gs,
                task_type=task_choice,
                score={},
                loaded_annotations={},
                RWNa_statistics={},
                annotation_statistics={},
                annotations=ann_file,
                todo=True
            )
            ann.save()
            load_annotation.delay(ann.id, task_choice.value)

            data = {
                'ann_id': ann.id,
                'message': "Your request is currently being processed",
            }

            return JsonResponse(data)
        else:
            errors = {}
            errors.update(ann_form.errors)
            data = errors

            return JsonResponse(data, status=400)
    else:
        gs_objects = [
            gs
            for gs in GoldenStandard.objects.all().order_by("-pub_date")
            if (gs.owner == request.user or gs.settings["scope"] == GSScopeEnum.public.value)
        ]

        gs_list = {}
        for gs_all in gs_objects:
            gs_list[gs_all.name] = {
                "is_simple_cta": gs_all.is_simple_cta(),
                "is_approximate_cta": gs_all.is_approximate_cta(),
            }

            if stiltool.settings.CHALLENGE_MODE:
                today = datetime.date.today()
                today_min = datetime.datetime.combine(today, datetime.time.min)
                today_max = datetime.datetime.combine(today, datetime.time.max)
                gs_list[gs_all.name]["max_uploads_today"] = gs_all.max_daily_uploads

                for task in gs_all.get_tasks_list():
                    ann_uploaded = UserAnnotation.objects.filter(
                        owner=request.user,
                        gs=gs_all,
                        _task_type=task.value,
                        added_date__range=(today_min, today_max),
                    ).count()

                    gs_list[gs_all.name][task.value] = {
                        "uploads_left": gs_all.max_daily_uploads - ann_uploaded,
                    }

        return render(request, 'app/load.html', {
            'annotation_form': ann_form,
            'tasks_available': json.dumps(ann_form.gs_tasks),
            'gs_list': json.dumps(gs_list),
            "challenge": stiltool.settings.CHALLENGE_MODE
        })


@login_required
def load_gs(request):
    if stiltool.settings.CHALLENGE_MODE and not request.user.is_staff:
        return redirect('leaderboard')

    gs_form = UploadGSForm(request.user, prefix="gs_form")

    if request.method == 'POST' and request.is_ajax():
        print(request.POST)
        gs_form = UploadGSForm(request.user, request.POST, request.FILES, prefix="gs_form")
        if gs_form.is_valid():
            # TODO: For now I manually save the golden standard
            gs_name = gs_form.cleaned_data.get('gs_name')
            cta = gs_form.cleaned_data.get('cta_file')
            cpa = gs_form.cleaned_data.get('cpa_file')
            cea = gs_form.cleaned_data.get('cea_file')
            cta_ancestors_file = gs_form.cleaned_data.get('cta_ancestors_file')
            cta_descendents_file = gs_form.cleaned_data.get('cta_descendents_file')
            tables = gs_form.cleaned_data.get('table_files')  # DEPRECATED
            scope = gs_form.cleaned_data.get('scope_choice')
            mode = gs_form.cleaned_data.get('mode_choice')

            GoldenStandard(
                owner=request.user,
                name=gs_name,
                settings={
                    "scope": scope,
                    "mode": mode
                },
                tasks_metadata=gs_form.metadata,
                cea=cea,
                cpa=cpa,
                cta=cta,
                ancestors=cta_ancestors_file,
                descendents=cta_descendents_file,
                tables=tables,
            ).save()

            data = {'message': f'Gold Standard "{gs_name}" has been added'}

            return JsonResponse(data)

        else:
            errors = {}
            errors.update(gs_form.errors)
            data = errors
            print(data)

            return JsonResponse(data, status=400)
    else:
        return render(request, 'app/gs_load.html', {
            'gs_form': gs_form,
            "challenge": stiltool.settings.CHALLENGE_MODE
        })


@login_required
def manage_gs(request):
    # TODO: Extract method: this is an utility function
    def redirect_params(url, params=None):
        response = redirect(url)
        if params:
            query_string = urllib.parse.urlencode(params)
            response['Location'] += '?' + query_string

        return response

    gs_objects = [
        gs
        for gs in GoldenStandard.objects.all()
        if gs.owner == request.user
    ]

    gs_list = {
        gs_all.name: {
            "availability": gs_all.settings["scope"],
            "mode": gs_all.settings["mode"],
        }
        for gs_all in gs_objects
    }

    gs_param = request.GET.get("gs", None)

    found = GoldenStandard.objects.filter(name=gs_param).count() > 0
    gs = None
    if found:
        gs = GoldenStandard.objects.filter(name=gs_param).first()
    else:
        print("gs wrong")
        if len(gs_objects) > 0:
            return redirect_params('manage-gs', {
                "gs": gs_objects[0].name,
            })
        # return redirect('index')

    if gs is not None:
        data = {
            "gs_name": gs.name,
            "gs_settings": {
                "availability": gs.settings["scope"],
                "mode": gs.settings["mode"],
            },
            "gs_max_daily_uploads": gs.max_daily_uploads,
            "form": ChangeGsSettings(instance=gs),
            "gs_id": gs.id,
        }
    else:
        data = {}

    has_no_data = len(data.keys()) == 0

    context = {
        'gs_list': gs_list,
        'data': data,
        'has_no_data': has_no_data,
        "challenge": stiltool.settings.CHALLENGE_MODE
    }

    return render(request, 'app/gs_management.html', context)


@login_required
def update_gs_mode(request):
    if request.is_ajax() and request.method == 'PUT':
        put = QueryDict(request.body)
        gsId = put.get("gsId", None)
        mode = put.get("mode", None)
        max_daily_uploads = put.get("max_daily_uploads", 0)

        if gsId is not None and mode is not None and mode in [e.value for e in GSModeEnum]:
            gs = GoldenStandard.objects.filter(id=gsId)
            if gs.count() > 0:
                if request.user == gs[0].owner:
                    tmp_gs = gs[0]
                    tmp_gs.settings["mode"] = mode
                    tmp_gs.max_daily_uploads = max_daily_uploads
                    tmp_gs.save()
                else:
                    return JsonResponse({"Status": "Bad gs id"}, status=400)
            else:
                return JsonResponse({"Status": "Bad gs id"}, status=400)

            return JsonResponse({"Status": "OK"})
        else:
            return JsonResponse({"Status": "Bad request"}, status=400)

    return redirect('index')


@login_required
def delete_gs(request):
    if request.is_ajax() and request.method == 'DELETE':
        delete = QueryDict(request.body)
        gsId = delete.get("gsId", None)

        if gsId is not None:
            gs = GoldenStandard.objects.filter(id=gsId)
            if gs.count() > 0:
                if request.user == gs[0].owner:
                    gs[0].delete()
                else:
                    return JsonResponse({"Status": "Bad gs id"}, status=400)
            else:
                return JsonResponse({"Status": "Bad gs id"}, status=400)

            return JsonResponse({"Status": "OK"})
        else:
            return JsonResponse({"Status": "Bad request"}, status=400)

    return redirect('index')


def get_table_stat(request):
    if request.method == 'GET' and request.is_ajax():
        ann_id_param = request.GET.get("idAnn", None)
        table_count_param = request.GET.get("dataCount", None)
        filter_param = request.GET.get("filter", "both")
        search_param = request.GET.get("search", None)  # Optional

        if filter_param not in ["both", "wrongs", "missings"]:
            return JsonResponse({"status": "bad request"})

        if search_param == "":
            search_param = None

        if ann_id_param is None or table_count_param is None:
            return JsonResponse({"status": "bad request"})

        ann_id_param = int(ann_id_param)
        table_count_param = int(table_count_param)

        if UserAnnotation.objects.filter(id=ann_id_param).count() > 0:
            ann = UserAnnotation.objects.filter(id=ann_id_param)[0]

            if ann.task_type == TasksEnum.CTA and not ann.gs.is_simple_cta() and not ann.gs.is_approximate_cta():  # AHAP
                tables_name = {}

                def search_predicate(table):
                    return search_param is not None and (search_param.lower() in table["table"].lower() or
                                                         search_param.lower() in table["value"]["perfect"][
                                                             "ann"].lower() or
                                                         any([search_param.lower() in w["ann"].lower() for w in
                                                              table["value"]["ok"]]) or
                                                         any([search_param.lower() in w.lower() for w in
                                                              table["value"]["wrong"]]))

                for a in ann.annotations:
                    if not a["table"] in tables_name:
                        tables_name[a["table"]] = dict()
                        tables_name[a["table"]]["wrongs"] = 0
                        tables_name[a["table"]]["total"] = 0
                        tables_name[a["table"]]["searchable"] = False

                    if filter_param == "wrongs" or filter_param == "both":
                        tables_name[a["table"]]["wrongs"] += len(a["value"]["wrong"])

                    if filter_param == "missings" or filter_param == "both":
                        if a["value"]["perfect"]["result"] == "missing":
                            tables_name[a["table"]]["wrongs"] += 1

                        for ok in a["value"]["ok"]:
                            if ok["result"] == "missing":
                                tables_name[a["table"]]["wrongs"] += 1

                    tables_name[a["table"]]["searchable"] = search_predicate(a) or tables_name[a["table"]]["searchable"]
                    tables_name[a["table"]]["total"] += len(a["value"]["ok"]) + len(a["value"]["perfect"])

                if search_param is not None:
                    tables_name = sorted(
                        list(filter(lambda item: item[1]["wrongs"] > 0 and item[1]["searchable"],
                                    list(tables_name.items()))),
                        key=lambda item: item[1]["wrongs"], reverse=True
                    )
                else:
                    tables_name = sorted(
                        list(filter(lambda item: item[1]["wrongs"] > 0, list(tables_name.items()))),
                        key=lambda item: item[1]["wrongs"], reverse=True
                    )

                if table_count_param >= len(tables_name):
                    return JsonResponse({"status": "bad table count"})

                my_table_name = tables_name[table_count_param][0]
                table = [
                    a
                    for a in ann.annotations
                    if a["table"] == my_table_name
                ]

                header = ["col", "perfect", "ok", "wrong"]
                header_vals = lambda row: {"col": row["col"]}

                rows = []
                for row in table:
                    rows.append([
                        {
                            **header_vals(row),
                            **row["value"]
                        }
                    ])

                data = {
                    "table_name": my_table_name,
                    "header": header,
                    "total_tables": len(tables_name),
                    "rows": rows
                }

            else:  # PRF1
                def search_predicate(table):
                    if search_param is not None:
                        if isinstance(table["value"], list):
                            searchable = any([search_param.lower() in w.lower() for w in table["value"]])
                            if "gs_annotations" in table["metadata"]:
                                searchable = searchable or any(
                                    [search_param.lower() in w.lower() for w in table["metadata"]["gs_annotations"]])
                        else:
                            searchable = search_param.lower() in table["value"].lower()
                            if "gs_annotations" in table["metadata"]:
                                searchable = searchable or any(
                                    [search_param.lower() in w.lower() for w in table["metadata"]["gs_annotations"]])

                        if filter_param == "wrongs":
                            return searchable and table["metadata"]["status"] == "wrong"
                        elif filter_param == "na":
                            return searchable and table["metadata"]["status"] == "na"
                        else:
                            return searchable and (
                                    table["metadata"]["status"] == "wrong" or table["metadata"]["status"] == "na")

                    return False

                tables_name = {}
                for a in ann.annotations:
                    if not a["table"] in tables_name:
                        tables_name[a["table"]] = dict()
                        tables_name[a["table"]]["wrongs"] = 0
                        tables_name[a["table"]]["total"] = 0
                        tables_name[a["table"]]["searchable"] = False

                    if a["metadata"]["status"] == "wrong":
                        if filter_param == "wrongs" or filter_param == "both":
                            tables_name[a["table"]]["wrongs"] += 1
                    elif a["metadata"]["status"] == "na":
                        if filter_param == "missings" or filter_param == "both":
                            tables_name[a["table"]]["wrongs"] += 1

                    tables_name[a["table"]]["searchable"] = search_predicate(a) or tables_name[a["table"]]["searchable"]
                    tables_name[a["table"]]["total"] += 1

                if search_param is not None:
                    tables_name = sorted(
                        list(filter(lambda item: item[1]["wrongs"] > 0 and item[1]["searchable"],
                                    list(tables_name.items()))),
                        key=lambda item: item[1]["wrongs"], reverse=True
                    )
                else:
                    tables_name = sorted(
                        list(filter(lambda item: item[1]["wrongs"] > 0, list(tables_name.items()))),
                        key=lambda item: item[1]["wrongs"], reverse=True
                    )

                if table_count_param >= len(tables_name):
                    return JsonResponse({"status": "bad table count"})

                my_table_name = tables_name[table_count_param][0]
                table = [
                    a
                    for a in ann.annotations
                    if a["table"] == my_table_name
                ]

                if ann.task_type == TasksEnum.CEA:
                    header = ["col", "row", "annotation", "gt"]
                    header_vals = lambda row: {"col": row["col"], "row": row["row"]}
                elif ann.task_type == TasksEnum.CPA:
                    header = ["head", "tail", "annotation", "gt"]
                    header_vals = lambda row: {"head": row["col_from"], "tail": row["col_to"]}
                elif ann.task_type == TasksEnum.CTA:
                    header = ["col", "annotation", "gt"]
                    header_vals = lambda row: {"col": row["col"]}

                rows = []
                for row in table:
                    if row["metadata"]["status"] == "wrong" and (filter_param == "wrongs" or filter_param == "both"):
                        if search_param is None:
                            rows.append([
                                {
                                    **header_vals(row),
                                    "annotation": {
                                        "url": row["value"],
                                        "metadata": row["metadata"]["status"]
                                    },
                                    "gt": row["metadata"]["gs_annotations"]
                                }
                            ])
                        else:
                            if search_predicate(row):
                                rows.append([
                                    {
                                        **header_vals(row),
                                        "annotation": {
                                            "url": row["value"],
                                            "metadata": row["metadata"]["status"]
                                        },
                                        "gt": row["metadata"]["gs_annotations"]
                                    }
                                ])
                    elif row["metadata"]["status"] == "na" and (filter_param == "missings" or filter_param == "both"):
                        if search_param is None:
                            rows.append([
                                {
                                    **header_vals(row),
                                    "annotation": {
                                        "url": "",
                                        "metadata": row["metadata"]["status"]
                                    },
                                    "gt": row["value"]
                                }
                            ])
                        else:
                            if search_predicate(row):
                                rows.append([
                                    {
                                        **header_vals(row),
                                        "annotation": {
                                            "url": "",
                                            "metadata": row["metadata"]["status"]
                                        },
                                        "gt": row["value"]
                                    }
                                ])

                data = {
                    "table_name": my_table_name,
                    "header": header,
                    "total_tables": len(tables_name),
                    "rows": rows
                }

            return JsonResponse(data)
        else:
            return JsonResponse({"status": "bad annotation id"})

    return JsonResponse({"status": "bad request"})


def changelog(request):
    context = {
        "challenge": stiltool.settings.CHALLENGE_MODE
    }
    return render(request, 'app/changelog.html', context)

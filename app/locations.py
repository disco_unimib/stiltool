import os

class Locations:
    @staticmethod
    def get_user_location(instance, filename):
        return os.path.join(instance.user.username, "avatar")

    @staticmethod
    def get_gt_directory(username):
        return os.path.join(username, os.path.join("gt"))

    @staticmethod
    def get_cea_location(instance, filename):
        return os.path.join(
            Locations.get_gt_directory(instance.owner.username), 
            os.path.join(instance.name, "CEA")
        )

    @staticmethod
    def get_cpa_location(instance, filename):
        return os.path.join(
            Locations.get_gt_directory(instance.owner.username),
            os.path.join(instance.name, "CPA")
        )

    @staticmethod
    def get_cta_location(instance, filename):
        return os.path.join(
            Locations.get_gt_directory(instance.owner.username),
            os.path.join(instance.name, "CTA")
        )

    @staticmethod
    def get_ancestors_location(instance, filename):
        return os.path.join(
            Locations.get_gt_directory(instance.owner.username),
            os.path.join(instance.name, "ancestors.json")
        )

    @staticmethod
    def get_descendents_location(instance, filename):
        return os.path.join(
            Locations.get_gt_directory(instance.owner.username),
            os.path.join(instance.name, "descendants.json")
        )

    @staticmethod
    def get_gt_tables_location(instance, filename):
        return os.path.join(
            Locations.get_gt_directory(instance.owner.username),
            "tables"
        )

    @staticmethod
    def get_anns_location(instance, filename):
        return os.path.join(
            instance.owner.username,
            os.path.join(
                "annotations",
                os.path.join(instance.gs.name, instance.task_type.value)
            )
        )
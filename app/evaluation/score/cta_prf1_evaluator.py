from app.evaluation.score.prf1_evaluator import PRF1Evaluator


class CTAPRF1Evaluator(PRF1Evaluator):
    def __init__(self, cta_annotation):
        PRF1Evaluator.__init__(self)

        self.annotation = cta_annotation

    def compute(self):
        self.compute_impl(self.annotation, ("table", "col"), "value")
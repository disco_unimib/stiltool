from abc import ABC, abstractmethod
from app.evaluation.utils import utils


class PRF1Evaluator(ABC):
    def __init__(self):
        self.score = {
            "P": {
                "label": "precision",
                "value": 0.0,
            },
            "R": {
                "label": "recall",
                "value": 0.0,
            },
            "F1": {
                "label": "f1",
                "value": 0.0,
            },
        }
        self.general_statistics = {
            "right": 0,
            "wrong": 0,
            "na": 0,
            "total": 0
        }
        self.__annotation_statistics = {}
        self.annotation_statistics = []
        self.evaluated_annotations = []
        self.annotated = 0
        self.targets = 0

    def eval_entry(self, gs_entry, gs_schema, gs_key, user_annotations):
        right = 0
        wrong = 0
        na = 0

        new_entry = {
            k: gs_entry[k]
            for k in gs_schema
        }

        if gs_key in user_annotations.keys():
            user_entry = user_annotations[gs_key][0]

            if user_entry not in self.__annotation_statistics:
                self.__annotation_statistics[user_entry] = {
                    "right": 0,
                    "wrong": 0,
                    "na": 0,
                    "total": 0
                }
            
            if user_entry.lower() in {v.lower() for v in gs_entry["value"]}:
                metadata = {"status": "right"}
                right += 1
                self.__annotation_statistics[user_entry]["right"] += 1
            else:
                metadata = {
                    "status": "wrong",
                    "gs_annotations": gs_entry["value"]
                }
                wrong += 1
                self.__annotation_statistics[user_entry]["wrong"] += 1

            self.__annotation_statistics[user_entry]["total"] += 1
            new_entry.update({
                "value": user_entry,
                "metadata": metadata
            })

            self.evaluated_annotations.append(new_entry)
        else:
            if gs_entry["value"][0] not in self.__annotation_statistics:
                self.__annotation_statistics[gs_entry["value"][0]] = {
                    "right": 0,
                    "wrong": 0,
                    "na": 0,
                    "total": 0
                }
            self.__annotation_statistics[gs_entry["value"][0]]["na"] += 1
            self.__annotation_statistics[gs_entry["value"][0]]["total"] += 1

            new_entry.update({
                "value": gs_entry["value"],
                "metadata": {"status": "na"}
            })
            self.evaluated_annotations.append(new_entry)
            na += 1

        return right, wrong, na

    def compute_scores_stats(self, right, wrong, na):
        self.general_statistics["right"] = right
        self.general_statistics["wrong"] = wrong
        self.general_statistics["na"] = na
        self.general_statistics["total"] = right + wrong + na

        self.annotated = right + wrong
        self.targets = right + wrong + na

        p, r, f1 = utils.compute_prf1_score(right, self.annotated, self.targets)
        self.score["P"]["value"] = p
        self.score["R"]["value"] = r
        self.score["F1"]["value"] = f1

    def compute_impl(self, annotation, keys, value_key):
        user_annotations = {
            tuple(ann[k] for k in keys): ann[value_key]
            for ann in annotation.annotations
        }
        gs_annotations = annotation.gs.get_task(annotation.task_type)

        right = 0
        wrong = 0
        na = 0
        for gs_entry in gs_annotations:
            gs_key = tuple(gs_entry[k] for k in keys)

            r, w, n = self.eval_entry(gs_entry, keys, gs_key, user_annotations)
            right += r
            wrong += w
            na += n

        self.annotation_statistics = [
            {**val, **{
                "annotation": annotation
            }}
            for annotation, val in self.__annotation_statistics.items()
        ]

        self.compute_scores_stats(right, wrong, na)

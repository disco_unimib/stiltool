from app.evaluation.score.prf1_evaluator import PRF1Evaluator


class CEAEvaluator(PRF1Evaluator):
    def __init__(self, cea_annotation):
        PRF1Evaluator.__init__(self)

        self.annotation = cea_annotation

    def compute(self):
        self.compute_impl(self.annotation, ("table", "col", "row"), "value")

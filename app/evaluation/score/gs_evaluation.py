from app.models import TasksEnum

 
class GSEvaluation:
    def __init__(self, annotation):
        self.annotation = annotation
        self.annotated = 0
        self.targets = 0
        self.score = None
        
    def compute(self):
        if self.annotation.task_type == TasksEnum.CPA:
            keys = ("table", "col_from", "col_to")
            value = "value"
        elif self.annotation.task_type == TasksEnum.CEA:
            keys = ("table", "col", "row")
            value = "value"
        elif self.annotation.task_type == TasksEnum.CTA:
            keys = ("table", "col")
            value = "concept"
        else:
            assert False
            
        if self.annotation.task_type == TasksEnum.CPA or self.annotation.task_type == TasksEnum.CEA:
            anns = {
                tuple(ann[k] for k in keys): { a.lower() for a in ann[value] }
                for ann in self.annotation.annotations
            }
            
            gs_anns = {
                tuple(ann[k] for k in keys): { a.lower() for a in ann[value] }
                for ann in self.annotation.gs.get_task(self.annotation.task_type)
            }
        else:
            anns = {
                tuple(ann[k] for k in keys): ann[value]
                for ann in self.annotation.annotations
            }
            
            gs_anns = {
                tuple(ann[k] for k in keys): ann[value]
                for ann in self.annotation.gs.get_task(self.annotation.task_type)
            }
        
        self.compute_annotated(anns, gs_anns)
        self.compute_targets(gs_anns)

        if self.annotation.task_type == TasksEnum.CTA:
            is_cta_simple = list(gs_anns.values())[0]["simple"]
        else:
            is_cta_simple = False

        if self.annotation.task_type == TasksEnum.CPA or self.annotation.task_type == TasksEnum.CEA or is_cta_simple:
            self.score = self.compute_PRF(anns, gs_anns)
        else:
            self.score = self.compute_AH_AP(anns, gs_anns)
    
    def compute_annotated(self, anns, gs_anns):
        self.annotated = 0
        for gs_key in gs_anns.keys():
            if gs_key in anns:
                self.annotated += 1
                
    def compute_targets(self, gs_anns):
        self.targets = len(gs_anns.keys())
        
    def compute_PRF(self, anns, gs_anns):
        # Precision = # correct cells / # annotated cells
        # Recall    = # correct cells / # target cells
        # F1        = (2 * Precision * Recall) / (Precision + Recall)
        
        def make_result(prec, recall, f1):
            assert (0.0 <= prec <= 1.0)
            assert (0.0 <= recall <= 1.0)
            assert (0.0 <= f1 <= 1.0)
            
            return {
                "precision": prec,
                "recall": recall,
                "f1": f1
            }

        correct_cells = 0
        for gs_key in gs_anns.keys():
            if gs_key in anns:
                if "best" in gs_anns[gs_key]:   # cta simple
                    print(set(anns[gs_key]), {gs_anns[gs_key]["best"]})
                    if len(set(anns[gs_key]).intersection({gs_anns[gs_key]["best"]})) > 0:
                        correct_cells += 1
                else:
                    if len(anns[gs_key].intersection(gs_anns[gs_key])) > 0:
                        correct_cells += 1
                   
        if self.annotated == 0 or self.targets == 0:
            return make_result(prec=0.0, recall=0.0, f1=0.0)
        
        precision = correct_cells / self.annotated
        recall = correct_cells / self.targets
        
        if precision + recall == 0.0:
            f1 = 0.0
        else:
            f1 = (2 * precision * recall) / (precision + recall)
        
        return make_result(prec=precision, recall=recall, f1=f1)
    
    def compute_AH_AP(self, anns, gs_anns):
        # AH = (1*(# perfect) + 0.5*(# okay) - 1*(# wrong)) / (# target columns)
        # AP = (# perfect) / (# annotated classes)
        def make_result(ah, ap):            
            return {
                "AH": ah,
                "AP": ap,
            }
        
        perfects = 0
        okay = 0
        wrong = 0
        annotated_classes = 0
        gs_classes = 0
        for gs_key in gs_anns.keys():
            if gs_key in anns:
                anns_list = {ann.lower() for ann in anns[gs_key]}
                annotated_classes += len(anns_list)
                
                gs_perfect = gs_anns[gs_key]["best"].lower()
                gs_okay = {ann_ok.lower() for ann_ok in gs_anns[gs_key]["ok"]}
                gs_classes += len(gs_okay) + 1
                
                if gs_perfect in anns_list:
                    perfects += 1
                    anns_list.discard(gs_perfect)

                okay += len(anns_list.intersection(gs_okay))
                wrong += len(anns_list.difference(gs_okay))
                
        print(perfects, okay, wrong)
        if annotated_classes == 0 or self.targets == 0:
            return make_result(ah=0.0, ap=0.0) 
                
        ah = (1*perfects + 0.5*okay - 1*wrong) / self.targets
        ap = perfects / annotated_classes
        
        return make_result(ah=ah, ap=ap)

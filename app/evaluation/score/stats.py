from app.models import TasksEnum


class AnnotationStats:
    def __init__(self, annotation):
        self.annotation = annotation

    def compute(self):
        if self.annotation.task_type == TasksEnum.CPA:
            keys = ("table", "col_from", "col_to")
            value = "value"
        elif self.annotation.task_type == TasksEnum.CEA:
            keys = ("table", "col", "row")
            value = "value"
        elif self.annotation.task_type == TasksEnum.CTA:
            keys = ("table", "col")
            value = "concept"
        else:
            assert False

        anns = {}
        gs_anns = {}
        if self.annotation.task_type == TasksEnum.CPA:
            anns = {
                tuple(ann[k] for k in keys): (ann[value][0].lower(), ann[value][0])
                for ann in self.annotation.annotations
            }

            gs_anns = {
                tuple(ann[k] for k in keys): (ann[value][0].lower(), ann[value][0])
                for ann in self.annotation.gs.get_task(self.annotation.task_type)
            }
        if self.annotation.task_type == TasksEnum.CEA:
            anns = {
                tuple(ann[k] for k in keys): (ann[value][0].lower(), ann[value][0])
                for ann in self.annotation.annotations
            }

            gs_anns = {
                tuple(ann[k] for k in keys): (
                    [a.lower() for a in ann[value]],
                    [a for a in ann[value]],
                )
                for ann in self.annotation.gs.get_task(self.annotation.task_type)
            }
        if self.annotation.task_type == TasksEnum.CTA:
            anns = {
                tuple(ann[k] for k in keys): ann[value]
                for ann in self.annotation.annotations
            }

            gs_anns = {
                tuple(ann[k] for k in keys): ann[value]
                for ann in self.annotation.gs.get_task(self.annotation.task_type)
            }

        completeness = None
        annotation_stat = None
        comparison = None
        if self.annotation.task_type == TasksEnum.CPA:
            completeness = self.compute_PRF_completeness(anns, gs_anns)
            annotation_stat = self.compute_CPA_stats(anns, gs_anns)
            comparison = self.compute_CPA_comparison(anns, gs_anns)
        elif self.annotation.task_type == TasksEnum.CEA:
            completeness = self.compute_PRF_completeness(anns, gs_anns)
            comparison = self.compute_CEA_comparison(anns, gs_anns)
        elif self.annotation.task_type == TasksEnum.CTA:
            completeness = self.compute_AH_AP_completeness(anns, gs_anns)
            annotation_stat = self.compute_CTA_stats(anns, gs_anns)

            # comparison = self.compute_CTA_comparison(anns, gs_anns)

        return completeness, annotation_stat, comparison

    def compute_annotated(self, anns, gs_anns):
        annotated = 0
        for gs_key in gs_anns.keys():
            if gs_key in anns:
                annotated += 1

        return annotated

    def compute_targets(self, gs_anns):
        return len(gs_anns.keys())

    def compute_PRF_completeness(self, anns, gs_anns):
        correct_cells = 0
        annotated = self.compute_annotated(anns, gs_anns)
        targets = self.compute_targets(gs_anns)
        for gs_key in gs_anns.keys():
            if gs_key in anns:
                my_ann = {anns[gs_key][0]}

                gs_annstmp = gs_anns[gs_key][0]

                if isinstance(gs_annstmp, list):
                    gs_annstmp = set(gs_annstmp)
                else:
                    gs_annstmp = {gs_annstmp}

                if len(my_ann.intersection(gs_annstmp)) > 0:
                    correct_cells += 1

        return self.make_PRF_global_stats(correct_cells, annotated, targets)

    def compute_AH_AP_completeness(self, anns, gs_anns):
        completeness = {
            "perfect": 0,
            "ok": 0,
            "wrong": 0,
            "NA": 0,
            "total": 0,
        }
        for gs_key in gs_anns.keys():
            if gs_key in anns:
                values = set(anns[gs_key])
                gs_values = {
                    gv
                    for gv in {gs_anns[gs_key]["best"]}.union(set(gs_anns[gs_key]["ok"]))
                    if gv != ""
                }

                gs_perfect = gs_anns[gs_key]["best"]
                gs_okay = {
                    a
                    for a in gs_anns[gs_key]["ok"]
                    if a != ""
                }

                if gs_perfect in values:
                    completeness["perfect"] += 1

                for g_ok in gs_okay:
                    if g_ok in values:
                        completeness["ok"] += 1

                completeness["NA"] += len(gs_values.difference(values))
                completeness["wrong"] += len(values.difference(gs_values))
                completeness["total"] += len(gs_values.union(values))

        return completeness

    def compute_CPA_stats(self, anns, gs_anns):
        stats = []
        predicates = {}
        for gs_key in gs_anns.keys():
            if gs_key in anns:
                value = anns[gs_key][0]
                value_printable = anns[gs_key][1]
                if value not in predicates.keys():
                    predicates[value] = {
                        "correct": 0,
                        "annotated": 0,
                        "targets": 0,
                        "name": value_printable,
                    }
                if len({anns[gs_key][0]}.intersection({gs_anns[gs_key][0]})) > 0:
                    predicates[value]["correct"] += 1
                predicates[value]["annotated"] += 1
            else:
                value = gs_anns[gs_key][0]
                value_printable = gs_anns[gs_key][1]
                predicates[value] = {
                    "correct": 0,
                    "annotated": 0,
                    "targets": 0,
                    "name": value_printable,
                }
            predicates[value]["targets"] += 1

        for pred in predicates:
            p = predicates[pred]

            annotated = p["annotated"]
            targets = p["targets"]
            if annotated > targets:
                annotated = targets

            s = {
                "name": p["name"]
            }
            s.update(self.make_PRF_global_stats(p["correct"], annotated, p["targets"]))
            stats.append(s)

        return stats

    def compute_CPA_comparison(self, anns, gs_anns):
        tables = {}
        for gs_key in gs_anns.keys():
            table_name, head, tail = gs_key

            if gs_key in anns:
                value_printable = anns[gs_key][1]

                if table_name not in tables.keys():
                    tables[table_name] = []

                correct = len(set(anns[gs_key]).intersection(set(gs_anns[gs_key]))) > 0
                tables[table_name].append({
                    "head": head,
                    "tail": tail,
                    "annotation": value_printable,
                    "gs": gs_anns[gs_key][1],
                    "correct": correct
                })

        return tables

    def compute_CEA_comparison(self, anns, gs_anns):
        tables = {}
        for gs_key in gs_anns.keys():
            table_name, col, row = gs_key

            if gs_key in anns:
                value_printable = anns[gs_key][1]

                if table_name not in tables.keys():
                    tables[table_name] = []

                correct = len(set(anns[gs_key]).intersection(set(gs_anns[gs_key][0]))) > 0
                if correct:
                    gs_anns_printable = anns[gs_key][1]
                else:
                    gs_anns_printable = " ".join(gs_anns[gs_key][1][0:3])

                tables[table_name].append({
                    "col": col,
                    "row": row,
                    "annotation": value_printable,
                    "gs": gs_anns_printable,
                    "correct": correct
                })

        return tables

    def compute_CTA_stats(self, anns, gs_anns):
        stats = []
        concepts = {}
        for gs_key in gs_anns.keys():
            if gs_key in anns:
                values = set(anns[gs_key])
                gs_values = {
                    gv
                    for gv in {gs_anns[gs_key]["best"]}.union(set(gs_anns[gs_key]["ok"]))
                    if gv != ""
                }

                for v in gs_values:
                    if v not in concepts.keys():
                        concepts[v] = {
                            "perfect": 0,
                            "ok": 0,
                            "wrong": 0,
                            "NA": 0,
                            "total": 0,
                        }

                gs_perfect = gs_anns[gs_key]["best"]
                gs_okay = {
                    a
                    for a in gs_anns[gs_key]["ok"]
                    if a != ""
                }

                if gs_perfect in values:
                    concepts[gs_perfect]["perfect"] += 1

                for g_ok in gs_okay:
                    if g_ok in values:
                        concepts[g_ok]["ok"] += 1

                diffNA = gs_values.difference(values)
                for el in diffNA:
                    concepts[el]["NA"] += 1

                diffW = values.difference(gs_values)
                for el in diffW:
                    if el not in concepts:
                        concepts[el] = {
                            "perfect": 0,
                            "ok": 0,
                            "wrong": 0,
                            "NA": 0,
                            "total": 0,
                        }
                    concepts[el]["wrong"] += 1

                for a in gs_values.union(values):
                    concepts[a]["total"] += 1

        for conc in concepts:
            c = concepts[conc]

            c["name"] = conc
            stats.append(c)

        return stats

    def make_PRF_global_stats(self, correct, annotated, targets):
        assert (correct >= 0)
        assert (annotated >= 0)
        assert (targets >= 0)

        return {
            "perfects": correct,
            "errors": max(annotated - correct, 0),
            "NA": max(targets - annotated, 0),
            "total": targets
        }

    def make_AH_AP_global_stats(self, perfects, okay, wrong, annotated, targets):
        assert (perfects >= 0)
        assert (okay >= 0)
        assert (wrong >= 0)
        assert (annotated >= 0)
        assert (targets >= 0)

        return {
            "perfects": perfects,
            "okay": okay,
            "errors": wrong,
            "NA": targets - annotated,
            "annotated": annotated,
            "total": targets
        }
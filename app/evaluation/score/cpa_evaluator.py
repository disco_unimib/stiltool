from app.evaluation.score.prf1_evaluator import PRF1Evaluator


class CPAEvaluator(PRF1Evaluator):
    def __init__(self, cpa_annotation):
        PRF1Evaluator.__init__(self)

        self.annotation = cpa_annotation

    def compute(self):
        self.compute_impl(self.annotation, ("table", "col_from", "col_to"), "value")
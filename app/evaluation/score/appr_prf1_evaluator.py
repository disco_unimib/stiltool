from abc import ABC, abstractmethod
from app.evaluation.utils import utils


class ApprPRF1Evaluator(ABC):
    def __init__(self):
        self.score = {
            "AP": {
                "label": "appr. precision",
                "value": 0.0,
            },
            "AR": {
                "label": "appr. recall",
                "value": 0.0,
            },
            "AF1": {
                "label": "appr. f1",
                "value": 0.0,
            },
        }
        self.general_statistics = {
            "right": 0,
            "wrong": 0,
            "ok": 0,
            "na": 0,
            "total": 0
        }
        self.__annotation_statistics = {}
        self.annotation_statistics = []
        self.evaluated_annotations = []
        self.annotated = 0
        self.targets = 0

    def eval_entry(self, gs_entry, gs_schema, gs_key, user_annotations, ancestors, descendents):
        right = 0
        ok = 0
        wrong = 0
        na = 0
        ann = 0
        targs = 0

        new_entry = {
            k: gs_entry[k]
            for k in gs_schema
        }

        if gs_key in user_annotations.keys():
            user_entries = user_annotations[gs_key]

            for user_entry in user_entries:
                if user_entry not in self.__annotation_statistics:
                    self.__annotation_statistics[user_entry] = {
                        "right": 0,
                        "wrong": 0,
                        "ok": 0,
                        "na": 0,
                        "total": 0
                    }

                if user_entry.lower() in {v.lower() for v in gs_entry["value"]}:
                    metadata = {"status": "right"}
                    right += 1
                    self.__annotation_statistics[user_entry]["right"] += 1
                elif user_entry.lower() in ancestors.get(gs_entry["value"][0].lower(), []):
                    depth = ancestors[gs_entry["value"][0].lower()][user_entry.lower()]
                    if depth <= 5:
                        score = pow(0.8, depth)
                        metadata = {"status": "ok"}
                        ok += score
                        self.__annotation_statistics[user_entry]["ok"] += 1
                    else:
                        score = 0.0
                        metadata = {
                            "status": "wrong",
                            "gs_annotations": gs_entry["value"]
                        }
                        wrong += 1
                        self.__annotation_statistics[user_entry]["wrong"] += 1
                elif user_entry.lower() in descendents.get(gs_entry["value"][0].lower(), []):
                    depth = descendents[gs_entry["value"][0].lower()][user_entry.lower()]
                    if depth <= 3:
                        score = pow(0.7, depth)
                        metadata = {"status": "ok"}
                        ok += score
                        self.__annotation_statistics[user_entry]["ok"] += 1     
                    else:
                        score = 0.0
                        metadata = {
                            "status": "wrong",
                            "gs_annotations": gs_entry["value"]
                        }
                        wrong += 1
                        self.__annotation_statistics[user_entry]["wrong"] += 1
                else:
                    metadata = {
                        "status": "wrong",
                        "gs_annotations": gs_entry["value"]
                    }
                    wrong += 1
                    self.__annotation_statistics[user_entry]["wrong"] += 1

                self.__annotation_statistics[user_entry]["total"] += 1

                new_entry = {
                    k: gs_entry[k]
                    for k in gs_schema
                }
                new_entry.update({
                    "value": user_entry,
                    "metadata": metadata
                })

                self.evaluated_annotations.append(new_entry)
                ann += 1
        else:
            if gs_entry["value"][0] not in self.__annotation_statistics:
                self.__annotation_statistics[gs_entry["value"][0]] = {
                    "right": 0,
                    "wrong": 0,
                    "ok": 0,
                    "na": 0,
                    "total": 0
                }
            self.__annotation_statistics[gs_entry["value"][0]]["na"] += 1
            self.__annotation_statistics[gs_entry["value"][0]]["total"] += 1

            new_entry.update({
                "value": gs_entry["value"],
                "metadata": {"status": "na"}
            })
            self.evaluated_annotations.append(new_entry)
            na += 1

        targs += 1
        return right, ok, wrong, na, ann, targs

    def compute_scores_stats(self, right, ok, wrong, na, annotated, targets):
        self.general_statistics["right"] = right
        self.general_statistics["wrong"] = wrong
        self.general_statistics["ok"] = ok
        self.general_statistics["na"] = na
        self.general_statistics["total"] = right + ok + wrong + na

        self.annotated = annotated
        self.targets = targets

        print(right + ok, self.annotated, self.targets)

        p, r, f1 = utils.compute_prf1_score(right + ok, self.annotated, self.targets)
        self.score["AP"]["value"] = p
        self.score["AR"]["value"] = r
        self.score["AF1"]["value"] = f1

    def compute_impl(self, annotation, keys, value_key):
        user_annotations = {
            tuple(ann[k] for k in keys): ann[value_key]
            for ann in annotation.annotations
        }
        gs_annotations = annotation.gs.get_task(annotation.task_type)
        ancestors = annotation.gs.ancestors
        descendents = annotation.gs.descendents

        right = 0
        ok = 0
        wrong = 0
        na = 0
        anns = 0
        targets = 0
        for gs_entry in gs_annotations:
            gs_key = tuple(gs_entry[k] for k in keys)

            r, o, w, n, ann, targ = self.eval_entry(gs_entry, keys, gs_key, user_annotations, ancestors, descendents)
            right += r
            ok += o
            wrong += w
            na += n
            anns += ann
            targets += targ

        self.annotation_statistics = [
            {**val, **{
                "annotation": annotation
            }}
            for annotation, val in self.__annotation_statistics.items()
        ]

        self.compute_scores_stats(right, ok, wrong, na, anns, targets)

class CTAApprPRF1Evaluator(ApprPRF1Evaluator):
    def __init__(self, cta_annotation):
        ApprPRF1Evaluator.__init__(self)

        self.annotation = cta_annotation

    def compute(self):
        print("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
        self.compute_impl(self.annotation, ("table", "col"), "value")

from abc import ABC
from app.evaluation.utils import utils


class AHAPEvaluator(ABC):
    def __init__(self):
        self.score = {
            "AH": {
                "label": "AH",
                "value": 0.0,
            },
            "AP": {
                "label": "AP",
                "value": 0.0,
            },
        }
        self.general_statistics = {
            "ah_max": 0,
            "ah_min": 0,
            "ah_curr": 0,
        }
        self.__annotation_statistics = {}
        self.annotation_statistics = []
        self.evaluated_annotations = []
        self.annotated = 0
        self.targets = 0

    def eval_entry(self, gs_entry, gs_schema, gs_key, user_annotations):
        perfect = 0
        okay = 0
        wrong = 0
        na = 0

        new_entry = {
            k: gs_entry[k]
            for k in gs_schema
        }

        if gs_key in user_annotations.keys():
            user_entries = user_annotations[gs_key]

            for user_entry in user_entries:
                if user_entry not in self.__annotation_statistics:
                    self.__annotation_statistics[user_entry] = {
                        "perfect": 0,
                        "okay": 0,
                        "wrong": 0,
                        "na": 0,
                        "total": 0
                    }

            user_entries_set = set(user_entries)
            user_entries_set_lower = {e.lower() for e in user_entries}
            metadata = {"perfect": {}, "ok": [], "wrong": []}
            
            metadata["perfect"]["ann"] = gs_entry["value"]["perfect"]
            if gs_entry["value"]["perfect"].lower() in user_entries_set_lower:
                metadata["perfect"]["result"] = "right"
                user_entries_set.remove(gs_entry["value"]["perfect"])
                perfect += 1

                self.__annotation_statistics[gs_entry["value"]["perfect"]]["perfect"] += 1
                self.__annotation_statistics[gs_entry["value"]["perfect"]]["total"] += 1
            else:
                metadata["perfect"]["result"] = "missing"
                na += 1

                if gs_entry["value"]["perfect"] not in self.__annotation_statistics:
                    self.__annotation_statistics[gs_entry["value"]["perfect"]] = {
                        "perfect": 0,
                        "okay": 0,
                        "wrong": 0,
                        "na": 0,
                        "total": 0
                    }
                self.__annotation_statistics[gs_entry["value"]["perfect"]]["na"] += 1
                self.__annotation_statistics[gs_entry["value"]["perfect"]]["total"] += 1

            for gs_entry_okay in gs_entry["value"]["ok"]:
                if gs_entry_okay.lower() in user_entries_set_lower:
                    metadata["ok"].append({
                        "ann": gs_entry_okay,
                        "result": "right"
                    })
                    user_entries_set.remove(gs_entry_okay)
                    okay += 1

                    self.__annotation_statistics[gs_entry_okay]["okay"] += 1
                    self.__annotation_statistics[gs_entry_okay]["total"] += 1
                else:
                    metadata["ok"].append({
                        "ann": gs_entry_okay,
                        "result": "missing"
                    })
                    na += 1

                    if gs_entry_okay not in self.__annotation_statistics:
                        self.__annotation_statistics[gs_entry_okay] = {
                            "perfect": 0,
                            "okay": 0,
                            "wrong": 0,
                            "na": 0,
                            "total": 0
                        }
                    self.__annotation_statistics[gs_entry_okay]["na"] += 1
                    self.__annotation_statistics[gs_entry_okay]["total"] += 1

            for user_entry in user_entries_set:
                metadata["wrong"].append(user_entry)
                wrong += 1

                self.__annotation_statistics[user_entry]["wrong"] += 1
                self.__annotation_statistics[user_entry]["total"] += 1

            new_entry.update({
                "value": metadata,
            })

            self.evaluated_annotations.append(new_entry)

        return perfect, okay, wrong, na

    def compute_scores_stats(self, perfect, okay, wrong, na, target_cols, gs_perfects, gs_okays):
        self.annotated = perfect + okay + wrong
        self.targets = perfect + okay + wrong + na

        ah, ap = utils.compute_ahap_score(perfect, okay, wrong, self.annotated, target_cols)

        gs_ah_max, gs_ap_max = utils.compute_ahap_score(gs_perfects, gs_okays, 0, self.targets, target_cols)
        gs_ah_min, gs_ap_min = utils.compute_ahap_score(0, 0, self.annotated, self.targets, target_cols)
        self.general_statistics["ah_max"] = gs_ah_max
        self.general_statistics["ah_min"] = gs_ah_min
        self.general_statistics["ah_curr"] = ah

        self.score["AH"]["value"] = ah
        self.score["AP"]["value"] = ap

    def compute_impl(self, annotation, keys, value_key):
        user_annotations = {
            tuple(ann[k] for k in keys): ann[value_key]
            for ann in annotation.annotations
        }
        gs_annotations = annotation.gs.get_task(annotation.task_type)

        perfect = 0
        okay = 0
        wrong = 0
        na = 0
        for gs_entry in gs_annotations:
            gs_key = tuple(gs_entry[k] for k in keys)

            p, o, w, n = self.eval_entry(gs_entry, keys, gs_key, user_annotations)
            perfect += p
            okay += o
            wrong += w
            na += n

        self.annotation_statistics = [
            {**val, **{
                "annotation": annotation
            }}
            for annotation, val in self.__annotation_statistics.items()
        ]

        target_cols = len(gs_annotations)
        gs_perfects = len(gs_annotations)
        gs_okays = sum([
            len(gs_entry["value"]["ok"])
            for gs_entry in gs_annotations
        ])
        self.compute_scores_stats(perfect, okay, wrong, na, target_cols, gs_perfects, gs_okays)


class CTAAHAPEvaluator(AHAPEvaluator):
    def __init__(self, cta_annotation):
        AHAPEvaluator.__init__(self)

        self.annotation = cta_annotation

    def compute(self):
        self.compute_impl(self.annotation, ("table", "col"), "value")

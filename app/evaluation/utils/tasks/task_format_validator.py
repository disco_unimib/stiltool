from abc import ABC, abstractmethod


class TaskFormatException(Exception):
    def __init__(self, message):
        super(TaskFormatException, self).__init__(message)


class TaskInvalidFormatException(TaskFormatException):
    def __init__(self, expected, found):
        super(TaskInvalidFormatException, self).__init__(
            f"Invalid number of fields: expected {expected}, found {found}")


class TaskBadNumericFieldsException(TaskFormatException):
    def __init__(self):
        super(TaskBadNumericFieldsException, self).__init__(f"Numeric field is not numeric")


class TaskFieldsConstraintsViolationException(TaskFormatException):
    def __init__(self):
        super(TaskFieldsConstraintsViolationException, self).__init__(f"Field constraints violation")


class TaskDuplicateEntriesException(TaskFormatException):
    def __init__(self):
        super(TaskDuplicateEntriesException, self).__init__(f"Duplicate entries")


class TaskNoDataLoadedException(TaskFormatException):
    def __init__(self):
        super(TaskNoDataLoadedException, self).__init__(f"No data loaded")


class TaskFormatValidator(ABC):
    def __init__(self, data: list):
        assert (isinstance(data, list))
        self._prefixes = ["http://www.wikidata.org/entity/", "http://dbpedia.org/resource/", "http://dbpedia.org/ontology/"]
        self.__data = data
        self.__error_code = (None, -1)

        super().__init__()

    def load(self):
        idx = 0
        try:
            if len(self.__data) > 0:
                for idx, row in enumerate(self.__data):
                    if isinstance(row, tuple):      # Ignore empty rows
                        self.load_row(row)
            else:
                raise TaskNoDataLoadedException()
        except TaskFormatException as e:
            self.__error_code = (str(e), idx + 1)

    def remove_prefix(self, text):
        for prefix in self._prefixes:
            if text.startswith(prefix):
                return text[len(prefix):]
        return text 

    def error(self):
        if self.__error_code[0] is not None:
            return f"Error: \"{self.__error_code[0]}\" at line {self.__error_code[1]}"

        return None

    @abstractmethod
    def load_row(self, row):
        pass

    @abstractmethod
    def get_data(self):
        pass

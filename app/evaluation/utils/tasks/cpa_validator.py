from .task_format_validator import *
import app.evaluation.utils.utils as check
from urllib.parse import unquote


class CPAValidator(TaskFormatValidator):
    def __init__(self, data):
        super().__init__(data)

        self.__cpa_data = []
        self.__headers = set()

    def load_row(self, data):
        table_name, col_from, col_to, predicate = self.cpa_clean(data)

        # Duplicate entry
        if (table_name, col_from, col_to) in self.__headers:
            raise TaskDuplicateEntriesException()

        self.__cpa_data.append({
            "table": table_name,
            "col_from": col_from,
            "col_to": col_to,
            "value": predicate
        })
        self.__headers.add((table_name, col_from, col_to))

    def cpa_clean(self, row):
        if len(row) < 4:
            raise TaskInvalidFormatException(expected=4, found=len(row))

        table_name, col_from, col_to, predicate = tuple(row[0:4])

        table_name = table_name.strip()
        col_from = col_from.strip()
        col_to = col_to.strip()

        if not check.is_integer(col_from) or not check.is_integer(col_to):
            raise TaskBadNumericFieldsException()

        col_from = int(col_from)
        col_to = int(col_to)
        predicate = [self.remove_prefix(unquote(predicate.strip()))]

        if len(table_name) == 0 or col_from < 0 or col_to < 0 or len(predicate) == 0:
            raise TaskFieldsConstraintsViolationException()

        return table_name, col_from, col_to, predicate

    def get_data(self):
        return self.__cpa_data

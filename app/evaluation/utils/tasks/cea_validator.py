from .task_format_validator import *
import app.evaluation.utils.utils as check
from urllib.parse import unquote


class CEAValidator(TaskFormatValidator):
    def __init__(self, data):
        super().__init__(data)

        self.__cea_data = []
        self.__headers = set()
    
    def load_row(self, data):
        # str, int, int, list
        table_name, col, row, anns = self.cea_clean(data)

        # Duplicate entry
        if (table_name, col, row) in self.__headers:
            raise TaskDuplicateEntriesException()

        self.__cea_data.append({
            "table": table_name,
            "col": col,
            "row": row,
            "value": anns
        })
        self.__headers.add((table_name, col, row))

    
    def cea_clean(self, row):
        if len(row) != 4:
            raise TaskInvalidFormatException(expected=4, found=len(row))

        # TODO
        #table_name, col, row, anns = row
        table_name, row, col, anns = row

        table_name = table_name.strip()
        col = col.strip()
        row = row.strip()

        if not check.is_integer(col) or not check.is_integer(row):
            raise TaskBadNumericFieldsException()

        col = int(col)
        row = int(row)
        anns = anns.strip()

       
        if len(table_name) == 0 or col < 0 or row < 0 or len(anns) == 0:
            raise TaskFieldsConstraintsViolationException()

        anns_list = list(set(anns.split(" ")))

        anns_list = [
            self.remove_prefix(unquote(ann))
            for ann in anns_list
        ]

        return table_name, col, row, anns_list

    def get_data(self):
        return self.__cea_data

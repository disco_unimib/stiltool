from urllib.parse import unquote

import app.evaluation.utils.utils as check
from .task_format_validator import *


class CTAGtPRF1Validator(TaskFormatValidator):
    def __init__(self, data):
        super().__init__(data)

        self.__cta_data = []
        self.__headers = set()

    def load_row(self, data):
        table_name, col, concepts = self.cta_gt_prf1_clean(data)

        # Duplicate entry
        if (table_name, col) in self.__headers:
            raise TaskDuplicateEntriesException()

        self.__cta_data.append({
            "table": table_name,
            "col": col,
            "value": concepts
        })
        self.__headers.add((table_name, col))

    def cta_gt_prf1_clean(self, row):
        if len(row) < 3:
            raise TaskInvalidFormatException(expected=3, found=len(row))

        table_name, col, concept = tuple(row[0:3])

        table_name = table_name.strip()
        col = col.strip()

        if not check.is_integer(col):
            raise TaskBadNumericFieldsException()

        col = int(col)
        concept = concept.strip()

        if len(table_name) == 0 or col < 0 or len(concept) == 0:
            raise TaskFieldsConstraintsViolationException()

        concepts_list = concept.split(" ")
        concepts_list = list(set(concepts_list))

        concepts_list = [
            self.remove_prefix(unquote(concept))
            for concept in concepts_list
        ]

        return table_name, col, concepts_list

    def get_data(self):
        return self.__cta_data


class CTAGtAPRFValidator(TaskFormatValidator):
    def __init__(self, data):
        super().__init__(data)
        self.__cta_data = []
        self.__headers = set()

    def load_row(self, data):
        table_name, col, perfect = self.cta_gt_prf1_clean(data)

        # Duplicate entry
        if (table_name, col) in self.__headers:
            raise TaskDuplicateEntriesException()

        self.__cta_data.append({
            "table": table_name,
            "col": col,
            "value": perfect
        })
        self.__headers.add((table_name, col))

    def cta_gt_prf1_clean(self, row):
        if len(row) < 3:
            raise TaskInvalidFormatException(expected=3, found=len(row))

        table_name, col, concept = tuple(row[0:3])

        table_name = table_name.strip()
        col = col.strip()

        if not check.is_integer(col):
            raise TaskBadNumericFieldsException()

        col = int(col)
        concept = concept.strip()

        if len(table_name) == 0 or col < 0 or len(concept) == 0:
            raise TaskFieldsConstraintsViolationException()

        concepts_list = concept.split(" ")
        concepts_list = list(set(concepts_list))

        concepts_list = [
            self.remove_prefix(unquote(concept))
            for concept in concepts_list
        ]

        return table_name, col, concepts_list

    def get_data(self):
        return self.__cta_data


class CTAGtAHAPValidator(TaskFormatValidator):
    def __init__(self, data):
        super().__init__(data)

        self.__cta_data = []
        self.__headers = set()

    def load_row(self, data):
        table_name, col, perfect, ok = self.cta_gt_ahap_clean(data)

        # Duplicate entry
        if (table_name, col) in self.__headers:
            raise TaskDuplicateEntriesException()

        self.__cta_data.append({
            "table": table_name,
            "col": col,
            "value": {
                "perfect": perfect,
                "ok": ok,
            },
        })
        self.__headers.add((table_name, col))

    def cta_gt_ahap_clean(self, row):
        if len(row) < 4:
            raise TaskInvalidFormatException(expected=4, found=len(row))

        table_name, col, perfect, oks = tuple(row[0:4])

        table_name = table_name.strip()
        col = col.strip()

        if not check.is_integer(col):
            raise TaskBadNumericFieldsException()

        col = int(col)
        perfect = perfect.strip()
        oks = oks.strip()

        if len(table_name) == 0 or col < 0 or len(perfect) == 0:
            raise TaskFieldsConstraintsViolationException()

        oks_list = list(set(oks.split(" ")))
        perfect = self.remove_prefix(unquote(perfect))
        ok = [
            self.remove_prefix(unquote(concept))
            for concept in oks_list
            if concept != ""
        ]

        return table_name, col, perfect, ok

    def get_data(self):
        return self.__cta_data


class CTAUserValidator(TaskFormatValidator):
    def __init__(self, data):
        super().__init__(data)

        self.__cta_data = []
        self.__headers = set()

    def load_row(self, data):
        table_name, col, concepts = self.cta_user_clean(data)

        # Duplicate entry
        if (table_name, col) in self.__headers:
            raise TaskDuplicateEntriesException()

        self.__cta_data.append({
            "table": table_name,
            "col": col,
            "value": concepts
        })
        self.__headers.add((table_name, col))
    
    def cta_user_clean(self, row):
        if len(row) < 3:
            raise TaskInvalidFormatException(expected=3, found=len(row))

        table_name, col, concepts = tuple(row[0:3])

        table_name = table_name.strip()
        col = col.strip()

        if not check.is_integer(col):
            raise TaskBadNumericFieldsException()

        col = int(col)
        concepts = concepts.strip()

        if len(table_name) == 0 or col < 0 or len(concepts) == 0:
            raise TaskFieldsConstraintsViolationException()

        concepts_list = concepts.split(" ")
        concepts_list = list(set(concepts_list))

        concepts_list = [
            self.remove_prefix(unquote(concept))
            for concept in concepts_list
        ]

        return table_name, col, concepts_list

    def get_data(self):
        return self.__cta_data

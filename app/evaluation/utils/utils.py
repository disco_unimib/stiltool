def is_integer(value):
    try:
        int(value)
    except ValueError:
        return False
    
    return True 


def compute_prf1_score(correct_cells, annotated_cells, target_cells):
    # Precision = # correct cells / # annotated cells
    # Recall    = # correct cells / # target cells
    # F1        = (2 * Precision * Recall) / (Precision + Recall)

    if annotated_cells == 0 or target_cells == 0:
        return 0.0, 0.0, 0.0

    precision = correct_cells / annotated_cells
    recall = correct_cells / target_cells

    if precision + recall == 0.0:
        f1 = 0.0
    else:
        f1 = (2 * precision * recall) / (precision + recall)

    return precision, recall, f1


def compute_ahap_score(perfect, okay, wrong, annotated_classes, target_columns):
    # AH = (1*(# perfect) + 0.5*(# okay) - 1*(# wrong)) / (# target columns)
    # AP = (# perfect) / (# annotated classes)

    if target_columns == 0.0:
        AH = 0.0
    else:
        AH = (1*perfect + 0.5*okay - 1*wrong) / target_columns
        
    if annotated_classes == 0.0:
        AP = 0.0
    else:
        AP = perfect / annotated_classes

    return AH, AP

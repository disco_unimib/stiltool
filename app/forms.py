import csv
import io
import json
import os

from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.core.files.images import get_image_dimensions
from django.core.files.uploadedfile import SimpleUploadedFile
from django.core.validators import FileExtensionValidator
from django.utils.safestring import mark_safe
from django.utils.html import conditional_escape

import app.evaluation.utils.tasks as tasks
from .models import GoldenStandard, TasksEnum, GSScopeEnum, GSModeEnum
import zipfile
from io import BytesIO

TASK_CHOICES = [
    (TasksEnum.CTA.name, "CTA: Column-Type Annotation"),
    (TasksEnum.CPA.name, "CPA: Columns-Property Annotation"),
    (TasksEnum.CEA.name, "CEA: Cell-Entity Annotation"),
]

SCOPE_CHOICES = [
    (GSScopeEnum.public.name, GSScopeEnum.public.value),
    (GSScopeEnum.private.name, GSScopeEnum.private.value),
]

MODE_CHOICES = [
    (GSModeEnum.info.name, GSModeEnum.info.value),
    (GSModeEnum.score.name, GSModeEnum.score.value),
]

CTA_SCORE_CHOICES = [
    ("PRF", mark_safe("Precision<br /> Recall<br /> F1 Score <br/> (PRF)")),
    ("APRF", mark_safe("APPROXIMATE<br /> Precision <br /> Recall<br /> F1 Score <br/> (AP-AR-AF)")),
    ("AHAP", mark_safe("AVERAGE <br />Hierarchical Score<br /> Perfect Score <br/> (AH-AP)"))
]


class ImageUploadForm(forms.Form):
    avatar = forms.ImageField(
        widget=forms.ClearableFileInput(),
        required=False
    )

    def clean_avatar(self):
        avatar = self.cleaned_data.get('avatar')

        if avatar:
            try:
                # validate content type
                main, sub = avatar.content_type.split('/')
                if not (main == 'image' and sub in ['jpg', 'jpeg', 'png']):
                    raise forms.ValidationError('Please upload a JPEG or PNG image')

                # validate file size
                if len(avatar) > (200 * 1024):
                    raise forms.ValidationError('Avatar file size may not exceed 200k.')

            except AttributeError:
                pass

        return avatar


class UploadAnnotationForm(forms.Form):
    def __init__(self, owner, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.owner = owner
        self.gs_objects = [
            gs
            for gs in GoldenStandard.objects.all()
            if gs.owner == self.owner or gs.settings["scope"] == GSScopeEnum.public.value
        ]
        self.gs_choices = [("", "")]
        self.gs_choices.extend([
            (gs.name, gs.name)
            for gs in self.gs_objects
        ])
        self.gs_tasks = {}

        for gs in self.gs_objects:
            self.gs_tasks[gs.name] = [e.value.lower() for e in gs.get_tasks_list()]

        # TODO: Bad
        self.fields['gs_choice'] = forms.ChoiceField(
            choices=self.gs_choices,
            required=False,
            label="Choose a Gold Standard *",
            label_suffix=""
        )

    annotation_file = forms.FileField(
        widget=forms.FileInput(attrs={'accept': '.csv'}),
        validators=[FileExtensionValidator(allowed_extensions=['csv'])],
        label="Choose your annotations file *",
        required=False,
        label_suffix="",
    )

    gs_choice = forms.ChoiceField(
        choices=[],
        widget=forms.Select(attrs={'class': 'form-control selectpicker'}),
        required=False
    )
    task_choice = forms.ChoiceField(choices=TASK_CHOICES, widget=forms.RadioSelect, required=False)

    def clean_annotation_file(self):
        data = self.cleaned_data.get('annotation_file', None)
        if data is None:
            raise forms.ValidationError("This field is required")

        return data

    def clean_gs_choice(self):
        data = self.cleaned_data.get('gs_choice', None)
        if data == "":
            raise forms.ValidationError("This field is required")

        return data

    def clean_task_choice(self):
        data = self.cleaned_data.get('task_choice', None)

        if data == "":
            raise forms.ValidationError("This field is required")

        if data == "CEA":
            return TasksEnum.CEA
        elif data == "CPA":
            return TasksEnum.CPA
        elif data == "CTA":
            return TasksEnum.CTA

        raise forms.ValidationError("Unknow error")

    # TODO: Extract method
    def clean(self):
        cleaned_data = super().clean()
        annotation_file = cleaned_data.get("annotation_file")
        task_choice = cleaned_data.get("task_choice")

        if annotation_file and task_choice:
            loader = self._load_task_data(annotation_file.read().decode('utf-8'), task_choice)

            error = loader.error()
            if error is not None:
                self.add_error('annotation_file', error)

            self.cleaned_data["annotation_file"] = self.zip_it(loader.get_data())

        return self.cleaned_data

    def zip_it(self, data):
        content = json.dumps(data, indent=4)

        in_memory = BytesIO()
        with zipfile.ZipFile(in_memory, "a", zipfile.ZIP_DEFLATED, False) as zf:
            zf.writestr("data", content)

            for f in zf.filelist:
                f.create_system = 0

        in_memory.seek(0)

        return SimpleUploadedFile("name", in_memory.read())

    # TODO: Move method, task should be enum
    def _load_task_data(self, ann_data, task):
        data = []

        csv_reader = csv.reader(
            io.StringIO(ann_data),
            delimiter=',',
            quotechar='"',
            skipinitialspace=True,
            quoting=csv.QUOTE_ALL
        )
        for row in csv_reader:
            if len(row) != 0:  # Skip empty rows
                data.append(tuple(row))

        if task.value == "CEA":
            loader = tasks.cea_validator.CEAValidator(data)
        elif task.value == "CPA":
            loader = tasks.cpa_validator.CPAValidator(data)
        elif task.value == "CTA":
            loader = tasks.cta_validator.CTAUserValidator(data)

        assert loader is not None
        loader.load()
        return loader


class UploadGSForm(forms.Form):
    def __init__(self, owner, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.owner = owner
        self.metadata = {}

    gs_name = forms.CharField(
        widget=forms.TextInput(),
        label="Gold Standard name",
        max_length=200,
        required=False,
        label_suffix=""
    )
    cta_file = forms.FileField(
        widget=forms.FileInput(attrs={'accept': '.csv'}),
        validators=[FileExtensionValidator(allowed_extensions=['csv'])],
        label="CTA annotations",
        required=False,
        label_suffix="",
        help_text='Format: "table id", "column id", "perfect annotation", "ok annotations (optional)"'
    )
    cpa_file = forms.FileField(
        widget=forms.FileInput(attrs={'accept': '.csv'}),
        validators=[FileExtensionValidator(allowed_extensions=['csv'])],
        label="CPA annotations",
        required=False,
        label_suffix="",
        help_text='Format: "table id", "head column id", "tail column id", "annotation"'
    )
    cea_file = forms.FileField(
        widget=forms.FileInput(attrs={'accept': '.csv'}),
        validators=[FileExtensionValidator(allowed_extensions=['csv'])],
        label="CEA annotations",
        required=False,
        label_suffix="",
        help_text='Format: "table id", "column id", "row id", "annotation"'
    )
    table_files = forms.FileField(
        widget=forms.ClearableFileInput(attrs={'multiple': True, 'accept': '.csv'}),
        validators=[FileExtensionValidator(allowed_extensions=['csv'])],
        label="Tables content files",
        required=False,
        label_suffix=""
    )
    cta_ancestors_file = forms.FileField(
        widget=forms.FileInput(attrs={'accept': '.json'}),
        validators=[FileExtensionValidator(allowed_extensions=['json'])],
        label="Ancestor distance",
        required=False,
        label_suffix="",
        help_text='Selected score require additional file'
    )

    cta_descendents_file = forms.FileField(
        widget=forms.FileInput(attrs={'accept': '.json'}),
        validators=[FileExtensionValidator(allowed_extensions=['json'])],
        label="Descendants distance",
        required=False,
        label_suffix="",
        help_text='Selected score require additional file'
    )

    scope_choice = forms.ChoiceField(choices=SCOPE_CHOICES, widget=forms.RadioSelect, required=False)
    mode_choice = forms.ChoiceField(choices=MODE_CHOICES, widget=forms.RadioSelect, required=False)
    cta_score_choice = forms.ChoiceField(choices=CTA_SCORE_CHOICES, initial=CTA_SCORE_CHOICES[0],
                                         widget=forms.RadioSelect, required=False)

    def clean_gs_name(self):
        name = self.cleaned_data.get("gs_name")

        if len(name) == 0:
            raise forms.ValidationError("Gold standard name is required")

        if name.startswith("."):
            raise forms.ValidationError("Gold standard name cannot start with a '.'")

        if os.path.basename(name) != name or "/" in name or "\\" in name:
            raise forms.ValidationError("Gold standard name cannot contains slashes")

        if "<" in name or ">" in name:
            raise forms.ValidationError("Gold standard name cannot contains <> symbols")

        return conditional_escape(name)

    def clean_max_daily_uploads(self):
        mdu = self.cleaned_data.get("max_daily_uploads")

        if mdu <= 0:
            raise forms.ValidationError("Value should be greater or equal to 1")

        return mdu

    def clean_scope_choice(self):
        scope = self.cleaned_data.get("scope_choice")

        if len(scope) == 0:
            raise forms.ValidationError("Choose an availability option")

        return scope

    def clean_mode_choice(self):
        mode = self.cleaned_data.get("mode_choice")

        if len(mode) == 0:
            raise forms.ValidationError("Choose a mode option")

        return mode

    def clean_cta_file(self):
        cta_file = self.cleaned_data.get("cta_file")

        if cta_file is not None:
            return self._load_task_data(cta_file.read().decode('utf-8'))

        return None

    def clean_cpa_file(self):
        cpa_file = self.cleaned_data.get("cpa_file")

        if cpa_file is not None:
            return self._load_task_data(cpa_file.read().decode('utf-8'))

        return None

    def clean_cea_file(self):
        cea_file = self.cleaned_data.get("cea_file")

        if cea_file is not None:
            return self._load_task_data(cea_file.read().decode('utf-8'))

        return None

    def clean_cta_ancestors_file(self):
        cta_ancestors = self.cleaned_data.get("cta_ancestors_file")

        if cta_ancestors is not None:
            return self._load_cta_distance("cta_ancestors_file", cta_ancestors.read().decode('utf-8'))

        return None

    def clean_cta_descendents_file(self):
        cta_descendents = self.cleaned_data.get("cta_descendents_file")

        if cta_descendents is not None:
            return self._load_cta_distance("cta_descendents_file", cta_descendents.read().decode('utf-8'))

        return None

    # TODO: Refactor this thing...
    def clean(self):
        cleaned_data = super().clean()

        name = cleaned_data.get("gs_name")
        cea_data = cleaned_data.get("cea_file", False)
        cpa_data = cleaned_data.get("cpa_file", False)
        cta_data = cleaned_data.get("cta_file", False)
        cta_score_choice = cleaned_data.get("cta_score_choice", None)
        cta_ancestors_file = cleaned_data.get("cta_ancestors_file", None)
        cta_descendents_file = cleaned_data.get("cta_descendents_file", None)
        scope = cleaned_data.get("scope_choice")
        mode = cleaned_data.get("mode_choice")

        # Private scope constrain for score mode
        if scope == GSScopeEnum.private.value and mode == GSModeEnum.score.value:
            raise forms.ValidationError("Invalid mode")

        # Unique constrain considering your uploads
        if not self._is_gs_unique(name):
            self.add_error('gs_name', "Name already in use")

        # Check if you uploaded at least one task
        if not any([cta_data, cpa_data, cea_data]):
            msg = "Add at least one task for gold standard"
            self.add_error('cta_file', msg)
            self.add_error('cpa_file', msg)
            self.add_error('cea_file', msg)

        # Prepare files from data
        if cta_data:
            if cta_score_choice == "APRF":
                if cta_ancestors_file is None:
                    self.add_error('cta_ancestors_file', "Add an ancestors file")
                    return cleaned_data
                elif cta_descendents_file is None:
                    self.add_error('cta_descendents_file', "Add a descendents file")

            cta_file = self._make_task_file("CTA", cta_data, cta_score_choice)
            cleaned_data["cta_file"] = cta_file
        if cpa_data:
            cpa_file = self._make_task_file("CPA", cpa_data, cta_score_choice)
            cleaned_data["cpa_file"] = cpa_file
        if cea_data:
            cea_file = self._make_task_file("CEA", cea_data, cta_score_choice)
            cleaned_data["cea_file"] = cea_file

        return cleaned_data

    def _make_task_file(self, task_name, task_data, cta_score_choice):
        if task_name == "CEA":
            loader = tasks.cea_validator.CEAValidator(task_data)
        elif task_name == "CPA":
            loader = tasks.cpa_validator.CPAValidator(task_data)
        elif task_name == "CTA":
            loader = None
            if len(task_data) > 0:
                if cta_score_choice == "AHAP":
                    loader = tasks.cta_validator.CTAGtAHAPValidator(task_data)
                    self.metadata = {"cta_format": "ahap"}
                elif cta_score_choice == "PRF":
                    loader = tasks.cta_validator.CTAGtPRF1Validator(task_data)
                    self.metadata = {"cta_format": "prf1"}
                elif cta_score_choice == "APRF":
                    loader = tasks.cta_validator.CTAGtAPRFValidator(task_data)
                    self.metadata = {"cta_format": "aprf"}

                """
                if len(task_data[0]) == 4:
                    if cta_score_choice == "AHAP":
                        loader = tasks.cta_validator.CTAGtAHAPValidator(task_data)
                        self.metadata = {"cta_format": "ahap"}
                    else:
                        self.add_error('cta_score_choice',
                                       "Invalid score for this task format: choose AH-AP")
                else:
                    if cta_score_choice == "PRF":
                        loader = tasks.cta_validator.CTAGtPRF1Validator(task_data)
                        self.metadata = {"cta_format": "prf1"}
                    elif cta_score_choice == "APRF":
                        loader = tasks.cta_validator.CTAGtAPRFValidator(task_data)
                        self.metadata = {"cta_format": "aprf"}
                    else:
                        self.add_error('cta_score_choice', "Invalid score for this task format: choose PRF or APRF")
                """

        if loader is not None:
            loader.load()
            error = loader.error()
            if error is not None:
                if task_name == "CTA":
                    self.add_error("cta_file", error)
                elif task_name == "CPA":
                    self.add_error("cpa_file", error)
                else:
                    self.add_error("cea_file", error)

            return SimpleUploadedFile("name", json.dumps(loader.get_data(), indent=4).encode("utf-8"))

        return None

    def _is_gs_unique(self, name):
        try:
            gs = GoldenStandard.objects.filter(name=name)
        except GoldenStandard.DoesNotExist:
            gs = []

        gs = [
            g
            for g in gs
            if g.owner == self.owner or g.settings["scope"] == GSScopeEnum.public.value
        ]

        return not len(gs) > 0

    # TODO: Move method, task should be enum
    def _load_task_data(self, ann_data):
        data = []

        csv_reader = csv.reader(
            io.StringIO(ann_data),
            delimiter=',',
            quotechar='"',
            skipinitialspace=True,
            quoting=csv.QUOTE_ALL
        )
        for row in csv_reader:
            if len(row) != 0:  # Skip empty rows
                data.append(tuple(row))

        return data

    def _load_cta_distance(self, field_name, distance_data):
        def is_integer(value):
            try:
                int(value)
            except ValueError:
                return False
            return True

        def remove_prefix(text):
            prefixes = ["http://www.wikidata.org/entity/", "http://dbpedia.org/resource/", "http://dbpedia.org/ontology/"]
            for prefix in prefixes:
                if text.startswith(prefix):
                    return text[len(prefix):]
            return text 

        try:
            data = json.loads(distance_data)
        except:
            self.add_error(field_name, "Upload a valid json file")
            return

        if not isinstance(data, dict):
            self.add_error(field_name, "Invalid ancestor/descendent format: root node must be of type 'map'")
            return

        if not all([
            isinstance(ent_map, dict)
            for entity, ent_map in data.items()
        ]):
            self.add_error(field_name, "Invalid ancestor/descentant format: child node(s) must be of type 'map'")
            return

        if not all([
            is_integer(distance)
            for entity, ent_map in data.items()
            for dist_entity, distance in ent_map.items()
        ]):
            self.add_error(field_name, "Invalid ancestor/descentant format: distance node(s) must be integer convertible")
            return

        if not all([
            int(distance) > 0
            for entity, ent_map in data.items()
            for dist_entity, distance in ent_map.items()
        ]):
            self.add_error(field_name, "Invalid ancestor/descentant format: distance node(s) must be positive non-zero integer")
            return

        normalized = {
            remove_prefix(entity): {
                remove_prefix(dist_entity).lower(): int(distance)
                for dist_entity, distance in ent_map.items()
            }
            for entity, ent_map in data.items()
        }
        
        return SimpleUploadedFile("distances", json.dumps(normalized, indent=4).encode("utf-8"))


class LoginForm(AuthenticationForm):
    def clean_username(self):
        username = self.cleaned_data.get("username")

        if len(username) == 0:
            raise forms.ValidationError("Username is required")

        return username

    def clean_password(self):
        password = self.cleaned_data.get("password")

        if len(password) == 0:
            raise forms.ValidationError("Password is required")

        return password


class RegistrationForm(UserCreationForm):
    email = forms.EmailField(max_length=200, help_text='Email', required=False)

    def clean_username(self):
        username = self.cleaned_data.get("username")

        if len(username) == 0:
            raise forms.ValidationError("Username is required")

        return username

    def clean_email(self):
        email = self.cleaned_data.get("email")

        if len(email) == 0:
            raise forms.ValidationError("This field is required.")

        return email

    def clean_password1(self):
        password1 = self.cleaned_data.get("password1")

        if len(password1) == 0:
            raise forms.ValidationError("Password is required")

        return password1


class ChangeGsSettings(forms.ModelForm):
    class Meta:
        model = GoldenStandard
        fields = ['settings', 'max_daily_uploads']

    mode_choice = forms.ChoiceField(choices=MODE_CHOICES, widget=forms.RadioSelect, required=False)
    scope_choice = forms.ChoiceField(choices=SCOPE_CHOICES, widget=forms.RadioSelect, required=False)
    max_daily_uploads_enabled = forms.BooleanField(required=False)
    max_daily_uploads = forms.IntegerField(
        label='Maximum daily uploads',
        label_suffix="",
        required=False,
        initial=-1,
        min_value=0
    )

    def __init__(self, *args, **kwargs):
        super(ChangeGsSettings, self).__init__(*args, **kwargs)

        instance = getattr(self, 'instance', None)

        if instance:
            set_daily_uploads = instance.max_daily_uploads > -1

            self.fields['max_daily_uploads_enabled'].initial = set_daily_uploads

            if not set_daily_uploads:
                self.fields['max_daily_uploads'].widget.attrs['disabled'] = "true"


class MessageForm(forms.Form):
    title = forms.CharField(
        label='News title',
        label_suffix="",
        required=True,
    )
    msg = forms.CharField(widget=forms.HiddenInput())

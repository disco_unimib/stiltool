from django.urls import path, re_path
import django
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('login/', views.login_user, name="login"),
    path('logout/', views.logout_user, name="logout"),
    path('register/', views.register, name="register"),
    path('profile/', views.profile, name="profile"),
    path('admin_console/', views.admin_console, name='admin_console'),
    path('board/', views.board, name='board'),
    path('annotations/', views.annotations, name='annotations'),
    path('leaderboard/', views.leaderboard, name='leaderboard'),
    path('evaluation/<int:id>', views.evaluation, name='evaluation'),
    path('load-annotations/', views.load, name='load'),
    path('load-gs/', views.load_gs, name='load-gs'),
    path('manage-gs/', views.manage_gs, name='manage-gs'),
    path('changelog/', views.changelog, name='changelog'),

    path('edit-user-status', views.edit_user_status, name='edit-user-status'),
    path('get-table-stat/', views.get_table_stat, name='get-table-stat'),
    path('delete-annotation/', views.delete_annotation, name='delete-annotation'),
    path('delete-gs/', views.delete_gs, name='delete-gs'),
    path('update-gs-mode/', views.update_gs_mode, name='update-gs-mode'),

    re_path(r'^account-activation/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        views.account_activation, name='account-activation'),
]
